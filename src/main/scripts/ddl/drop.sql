--------------------------------------------------------
--  DDL for Droping Tables
--------------------------------------------------------
DROP TABLE "COMMENTS";
DROP TABLE "NEWS_AUTHOR";
DROP TABLE "AUTHOR";
DROP TABLE "NEWS_TAG";
DROP TABLE "TAG";
DROP TABLE "NEWS";
DROP TABLE "ROLES";
DROP TABLE "USER";
--------------------------------------------------------
--  DDL for Droping Sequences
--------------------------------------------------------
DROP SEQUENCE "AUTHOR_AUTHOR_ID_SEQ";
DROP SEQUENCE "COMMENTS_COMMENT_ID_SEQ";
DROP SEQUENCE "NEWS_NEWS_ID_SEQ";
DROP SEQUENCE "TAG_TAG_ID_SEQ";
DROP SEQUENCE "USER_USER_ID_SEQ";
--------------------------------------------------------
--  DDL for Droping Views
--------------------------------------------------------
DROP VIEW "NEWS_TAG_NAME";