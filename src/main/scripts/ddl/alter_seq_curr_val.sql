create or replace procedure SET_SEQ_VAL(seq_name in varchar2, seq_val in number)
is
   last_val number;
begin
   execute immediate 'select ' || seq_name || '.nextval from dual' INTO last_val;
   execute immediate 'alter sequence ' || seq_name || ' increment by -' || (last_val - seq_val + 1);
   execute immediate 'select ' || seq_name || '.nextval from dual' INTO last_val;
   execute immediate 'alter sequence ' || seq_name || ' increment by 1';
end;