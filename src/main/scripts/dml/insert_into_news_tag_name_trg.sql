create or replace trigger insert_into_news_tag_name_trg
instead of insert on news_tag_name
for each row
declare
  ins_tag_id NUMBER(20);
begin
  select tag_id into ins_tag_id from tag where tag_name = :new.tag_name;
  if ins_tag_id is null then
    insert into tag (tag_name) values (:new.tag_name)
    returning tag_id into ins_tag_id;
  end if;
  insert into news_tag (news_id, tag_id) values (:new.news_id, ins_tag_id);
end insert_into_news_tag_name_trg;