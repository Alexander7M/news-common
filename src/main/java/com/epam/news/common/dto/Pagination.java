package com.epam.news.common.dto;

public class Pagination {
	
	public final int pageSize;
	public final int pageNumber;
	
	public Pagination(int pageNumber) {
		this.pageSize = 10;
		this.pageNumber = pageNumber;
	}
	
	public Pagination(int pageNumber, int pageSize) {
		this.pageSize = pageSize;
		this.pageNumber = pageNumber;
	}
	
	public long getFirstIndex() {
		return (pageNumber-1) * pageSize + 1;
	}
	
	public long getLastIndex() {
		return pageNumber * pageSize;
	}
	
}
