package com.epam.news.common.dto;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class NewsFilter {

	private Long requiredAuthorId;
	private HashSet<Long> specifiedTagIds = new HashSet<>();
	
	public NewsFilter(Long authorId, Collection<Long> tagIds) {
		this.requiredAuthorId = authorId;
		if (tagIds != null) {
			this.specifiedTagIds.addAll(tagIds);
		}
	}
	
	public Long getRequiredAuthor() {
		return requiredAuthorId;
	}
	
	public void setRequiredAuthor(Long requiredAuthorId) {
		this.requiredAuthorId = requiredAuthorId;
	}
	
	public boolean hasTagConstraint() {
		return specifiedTagIds != null && !specifiedTagIds.isEmpty();
	}
	
	public Set<Long> getSpecifiedTags() {
		return specifiedTagIds;
	}
	
	public void setSpecifiedTags(Collection<Long> specifiedTagIds) {
		this.specifiedTagIds.clear();
		this.specifiedTagIds.addAll(specifiedTagIds);
	}
	
	public void addSpecifiedTag(Long specifiedTagId) {
		specifiedTagIds.add(specifiedTagId);
	}
	
}
