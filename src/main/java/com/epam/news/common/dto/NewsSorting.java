package com.epam.news.common.dto;

import static com.epam.news.common.dto.OrderType.*;

public class NewsSorting {

	public enum SortingType {
		WITHOUT_ANY_SORTING,
		COMMENT_NUMBER_SORTING,
		CREATION_DATE_SORTING;
	}
	
	public final SortingType type;
	public final OrderType order;
	
	public NewsSorting() {
		this.type = SortingType.COMMENT_NUMBER_SORTING;
		this.order = DESC;
	}
	
	public NewsSorting(SortingType soringType) {
		this.type = soringType;
		this.order = DESC;
	}
	
	public NewsSorting(SortingType soringType, OrderType orderType) {
		this.type = soringType;
		this.order = orderType;
	}
	
}

