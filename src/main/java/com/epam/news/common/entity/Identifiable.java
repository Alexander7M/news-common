package com.epam.news.common.entity;

import java.io.Serializable;

public interface Identifiable<I> extends Serializable {
	
	I getId();
	void setId(I id);
	
	default boolean isIdentified() {
		return getId() != null;
	}
	
}
