package com.epam.news.common.entity;

/*
 * #TODO = chage realication to immutable
 * this is value object 
 */
public class Tag implements Identifiable<Long> {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String name;
	
	public Tag() {
		
	}
	
	public Tag(Long id) {
		this.id = id;
	}
	
	public Tag(String name) {
		this.name = name;
	}
	
	public Tag(Long id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		if (id != null ) {
			return id.hashCode();
		} else {
			if (name != null) {
				return name.hashCode();
			} else {
				return 0;
			}
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tag other = (Tag) obj;
		if (id != null && other.id != null) {
			return id.equals(other.id);
		} else {
			if (name != null) {
				return name.equals(other.name);
			} else {
				return false;
			}
		}
	}

	@Override
	public String toString() {
		return "Tag {\n"
				+ "\tid = " + id + ",\n"
				+ "\tname = " + name + "\n"
				+ "}";
	}
	
	
	
}
