package com.epam.news.common.entity;

import java.io.Serializable;
import java.time.Instant;

public class Comment implements Identifiable<Long>, Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Long newsId;
	private String text;
	private Instant creationDate;
	
	public Comment() {
		this.creationDate = Instant.now();
	}
	
	public Comment(Long id) {
		this.id = id;
		this.creationDate = Instant.now();
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public Instant getCreationDate() {
		return creationDate;
	}
	
	public void setCreationDate(Instant creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public int hashCode() {
		if (id != null) {
			return id.hashCode();
		} else {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
			result = prime * result + ((text == null) ? 0 : text.hashCode());
			result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
			return result;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (id != null) {
			return id.equals(other.id);
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return "Comment [\n"
				+ "\tid = " + id + ", \n"
				+ "\tnewsId = " + newsId + ", \n"
				+ "\ttext = " + text + ", \n"
				+ "\tcreationDate = " + creationDate + "\n"
				+ "]";
	}
	
	
	
}
