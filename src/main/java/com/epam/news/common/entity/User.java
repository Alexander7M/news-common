package com.epam.news.common.entity;

public class User implements Identifiable<Long> {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String name;
	private String login;
	private String password;
	private UserRole role; 
	
	public User() {
		
	}
	
	public User(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public UserRole getRole() {
		return role;
	}

	public void setRole(UserRole role) {
		this.role = role;
	}

	@Override
	public int hashCode() {
		if (id != null) {
			return id.hashCode();
		} else if (login != null) {
			return login.hashCode();
		} else {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((name == null) ? 0 : name.hashCode());		
			result = prime * result + ((role == null) ? 0 : role.hashCode());
			return result;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (id != null) {
			return id.equals(other.id);
		} else {
			if (login != null) {
				return login.equals(login);
			} else {
				return false;
			}
		}
	}
	
	
	
}
