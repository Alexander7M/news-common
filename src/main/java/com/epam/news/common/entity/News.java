package com.epam.news.common.entity;

import java.io.Serializable;
import java.time.Instant;
import java.util.Date;

public class News implements Identifiable<Long>, Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String title;
	private String shortText;
	private Instant creationDate;
	private Date modificationDate;
	private String fullText;
	
	public News() {
		
	}
	
	public News(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public Instant getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Instant creationDate) {
		this.creationDate = creationDate;
	}
	
	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	@Override
	public int hashCode() {
		if (id != null) {
			return id.hashCode();
		} else {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((title == null) ? 0 : title.hashCode());
			result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
			result = prime * result + ((modificationDate == null) ? 0 : modificationDate.hashCode());
			return result;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		News other = (News) obj;
		if (id != null) {
			return id.equals(other.id);
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return "News {\n"
				+ "\tid = " + id + ", \n"
				+ "\ttitle = " + title + ", \n"
				+ "\tshortText = " + shortText + ", \n"
				+ "\tcreationDate = " + creationDate + ", \n"
				+ "\tmodificationDate = " + modificationDate + ", \n"
				+ "\tfullText = " + fullText + "\n"
				+ "}";
	}
	
}
