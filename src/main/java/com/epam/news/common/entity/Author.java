package com.epam.news.common.entity;

import java.io.Serializable;
import java.time.Instant;

public class Author implements Identifiable<Long>, Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String name;
	private Instant expirationDate;
	
	public Author() {
		
	}
	
	public Author(Long id) {
		this.id = id;
	}

	public Author(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Instant getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Instant expirationInstant) {
		this.expirationDate = expirationInstant;
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public void setExpired() {
		this.expirationDate = Instant.now();
	}
	
	public boolean isExpired() {
		return expirationDate != null;
	}
	
	@Override
	public int hashCode() {
		if(id != null) {
			return id.hashCode();
		} else {	
			final int prime = 31;
			int nameHashCode = name != null ? name.hashCode() : 0;
			int expirationInstantHashCode = expirationDate != null ? expirationDate.hashCode() : 0;
			return prime * nameHashCode + expirationInstantHashCode;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;
		if (id == null) {
			return false;
		} else {
			return id.equals(other.id);
		}
	}

	@Override
	public String toString() {
		return "Author "+ "{\n"
				+ "\tid = " + id + ", \n"+
				"\tname = " + name + ", \n"+
				"\texpirationInstant = " + expirationDate + "\n"
				+ "}";
	}
	
}
