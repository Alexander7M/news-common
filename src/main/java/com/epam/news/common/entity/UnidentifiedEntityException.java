package com.epam.news.common.entity;

public class UnidentifiedEntityException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public UnidentifiedEntityException(Class<? extends Identifiable<?>> clazz) {
		super(clazz.getSimpleName() + " entity is unidentified.");
	}
	
	public UnidentifiedEntityException(Identifiable<?> entity) {
		super(entity.getClass().getSimpleName() + " entity is unidentified.\nEntity: " + entity.toString());
	}
	
	public UnidentifiedEntityException() {
		super();
	}

	public UnidentifiedEntityException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public UnidentifiedEntityException(String message, Throwable cause) {
		super(message, cause);
	}

	public UnidentifiedEntityException(String message) {
		super(message);
	}

	public UnidentifiedEntityException(Throwable cause) {
		super(cause);
	}

	
	
}
