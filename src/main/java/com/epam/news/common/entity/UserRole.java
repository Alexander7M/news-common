package com.epam.news.common.entity;

public enum UserRole {
	
	GUEST,
	READER,
	AUTHOR,
	ADMIN;
	
}
