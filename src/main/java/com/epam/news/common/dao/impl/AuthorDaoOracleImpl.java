package com.epam.news.common.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import com.epam.news.common.dao.AuthorDao;
import com.epam.news.common.dao.exception.DataAccessException;
import com.epam.news.common.dao.impl.util.IndexedStatementWriter;
import com.epam.news.common.dao.impl.util.TableMapper;
import com.epam.news.common.entity.Author;

public class AuthorDaoOracleImpl extends TransactionalDao implements AuthorDao {
	
	private static final String ID_COLUMN = "AUTHOR_ID";
	private static final String NAME_COLUMN = "AUTHOR_NAME";
	private static final String EXPIRED_COLUMN = "EXPIRED";
	
	public static class AuthorTableMapper implements TableMapper<Author> {
		@Override
		public Author mapRow(ResultSet resultSet) throws SQLException {
			Author author = new Author();
			author.setId(getLong(resultSet, ID_COLUMN));
			author.setName(getString(resultSet, NAME_COLUMN));
			author.setExpirationDate(getInstant(resultSet, EXPIRED_COLUMN));
			return author;
		}
	}
	
	private static final String INSERT_AUTHOR_SQL_QUERY = "INSERT INTO AUTHOR(AUTHOR_NAME) VALUES(?)";
	private static final String DELETE_AUTHOR_SQL_QUERY = "DELETE FROM AUTHOR WHERE AUTHOR_ID = ?";
	private static final String UPDATE_AUTHOR_SQL_QUERY = "UPDATE AUTHOR SET AUTHOR_NAME = ?, EXPIRED = ? WHERE AUTHOR_ID = ?";
	private static final String UPDATE_AUTHOR_NAME_SQL_QUERY = "UPDATE AUTHOR SET AUTHOR_NAME = ? WHERE AUTHOR_ID = ?";
	private static final String UPDATE_EXPIRED_FIELD_SQL_QUERY = "UPDATE AUTHOR SET EXPIRED = ? WHERE AUTHOR_ID = ?";
	private static final String SELECT_ALL_AYTHORS_SQL_QUERY = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHOR";
	private static final String SELECT_AUTHOR_BY_ID_SQL_QUERY = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHOR WHERE AUTHOR_ID = ?";
	private static final String SELECT_AUTHOR_BY_NAME_SQL_QUERY =
			"SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED " + 
			"FROM AUTHOR "+
			"WHERE AUTHOR_NAME = ?";
	
	private final IndexedStatementWriter indexedStatementWriter = new IndexedStatementWriter();
	private final TableMapper<Author> authorReader = new AuthorTableMapper();
	private final TableMapper<Long> idReader = (resultSet) -> {return resultSet.getLong(1);};
	
	@Override
	public List<Author> selectAllWithCommentCountSorting() throws DataAccessException {
		Connection connection = getConnection();
		try (Statement statement = connection.createStatement()) {
			ResultSet resultSet = statement.executeQuery(SELECT_ALL_AYTHORS_SQL_QUERY);
			return authorReader.mapTable(resultSet);
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}

	@Override
	public Author selectByKey(Long id) throws DataAccessException {
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(SELECT_AUTHOR_BY_ID_SQL_QUERY)) {
			indexedStatementWriter.write(statement, 1, id);
			ResultSet resultSet = statement.executeQuery();
			return authorReader.mapFirstRow(resultSet);
		}
		catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}

	@Override
	public List<Author> selectByName(String authorName) throws DataAccessException {
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(SELECT_AUTHOR_BY_NAME_SQL_QUERY)) {
			indexedStatementWriter.write(statement, 1, authorName);
			ResultSet resultSet = statement.executeQuery();
			return authorReader.mapTable(resultSet);
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
	
	@Override
	public Long create(Author author) throws DataAccessException {
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(INSERT_AUTHOR_SQL_QUERY, new String[] { "AUTHOR_ID" })) {
			indexedStatementWriter.write(statement, 1, author.getName());
			statement.executeUpdate();
			ResultSet keySet = statement.getGeneratedKeys();
			return idReader.mapFirstRow(keySet);
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
	
	@Override
	public void delete(Long id) throws DataAccessException {
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(DELETE_AUTHOR_SQL_QUERY)) {
			indexedStatementWriter.write(statement, 1, id);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
	
	@Override
	public void update(Author author) throws DataAccessException {
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(UPDATE_AUTHOR_SQL_QUERY)) {
			indexedStatementWriter.write(statement, 1, author.getName());
			indexedStatementWriter.write(statement, 2, author.getExpirationDate());
			indexedStatementWriter.write(statement, 3, author.getId());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
	
	@Override
	public boolean updateNameField(Author author) throws DataAccessException {
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(UPDATE_AUTHOR_NAME_SQL_QUERY)) {
			indexedStatementWriter.write(statement, 1, author.getName());
			indexedStatementWriter.write(statement, 1, author.getId());
			return statement.executeUpdate() == 1;
		}
		catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
	
	@Override
	public boolean updateExpiredField(Author author) throws DataAccessException {
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(UPDATE_EXPIRED_FIELD_SQL_QUERY)) {
			indexedStatementWriter.write(statement, 1, author.getExpirationDate());
			indexedStatementWriter.write(statement, 2, author.getId());
			return statement.executeUpdate() == 1;
		}
		catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
	
}
