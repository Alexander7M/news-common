package com.epam.news.common.dao;

import com.epam.news.common.entity.User;

public interface UserDao extends CrudDao<Long, User> {
	
}