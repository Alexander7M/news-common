package com.epam.news.common.dao.impl.util;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.Instant;

public class NamedStatementWriter {

	private static class NamedStatementWriterException extends SQLException {
		static final long serialVersionUID = 1L;
		public NamedStatementWriterException(String paramName, Integer paramIndex, SQLException cause) {
			super(String.format("Exception while setting statement parameter %s with index %d", paramName, paramIndex), cause);
		}
	}
	
	private static final IndexedStatementWriter indexedStatementWriter = new IndexedStatementWriter();
	
	private PreparedStatement statement;
	private IndexProxy indexProxy;
	
	public NamedStatementWriter(PreparedStatement statement, IndexProxy indexProxy) {
		this.statement = statement;
		this.indexProxy = indexProxy;
	}
	
	public void setLongParameter(String paramName, Long paramValue) throws SQLException {
		int paramIndex = indexProxy.getIndex(paramName);
		try {
			indexedStatementWriter.write(statement, paramIndex, paramValue);
		} catch (SQLException e) {
			throw new NamedStatementWriterException(paramName, paramIndex, e);
		}
	}
	
	public void setIntegerParameter(String paramName, Long paramValue) throws SQLException {
		int paramIndex = indexProxy.getIndex(paramName);
		try {
			indexedStatementWriter.write(statement, paramIndex, paramValue);
		} catch (SQLException e) {
			throw new NamedStatementWriterException(paramName, paramIndex, e);
		}
	}
	
	public void setStringParameter(String paramName, String paramValue) throws SQLException {
		int paramIndex = indexProxy.getIndex(paramName);
		try {
			indexedStatementWriter.write(statement, paramIndex, paramValue);
		} catch (SQLException e) {
			throw new NamedStatementWriterException(paramName, paramIndex, e);
		}
	}
	
	public void setInstantParameter(String paramName, Instant paramValue) throws SQLException {
		int paramIndex = indexProxy.getIndex(paramName);
		try {
			indexedStatementWriter.write(statement, paramIndex, paramValue);
		} catch (SQLException e) {
			throw new NamedStatementWriterException(paramName, paramIndex, e);
		}
	}
	
	public void setDateParameter(String paramName, java.util.Date paramValue) throws SQLException {
		int paramIndex = indexProxy.getIndex(paramName);
		try {
			indexedStatementWriter.write(statement, paramIndex, paramValue);
		} catch (SQLException e) {
			throw new NamedStatementWriterException(paramName, paramIndex, e);
		}
	}
	
	public void setObjectParameter(String paramName, Object paramValue) throws SQLException {
		int paramIndex = indexProxy.getIndex(paramName);
		try {
			indexedStatementWriter.write(statement, paramIndex, paramValue);
		} catch (SQLException e) {
			throw new NamedStatementWriterException(paramName, paramIndex, e);
		}
	}
	
	public <T> void setIterableParameter(String paramName, Iterable<T> paramValue) throws SQLException {
		int paramIndex = indexProxy.getIndex(paramName);
		try {
			indexedStatementWriter.write(statement, paramIndex, paramValue);
		} catch (SQLException e) {
			throw new NamedStatementWriterException(paramName, paramIndex, e);
		}
	}
	
}
