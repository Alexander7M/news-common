package com.epam.news.common.dao.impl.util;

import java.util.regex.Pattern;

public class SQLQueryBuilder {
	
	// #TODO = VERY VERY BAD. is doesn't work is query ends with '?'.
	public static String expandJokerToArray(String query, int jokerIdnex, int arraySize) {
		String jokerSequence = getJokerSequene(arraySize);
		StringBuilder builder = new StringBuilder(query.length() - 1 + jokerSequence.length());
		String[] parts = Pattern.compile("\\?").split(query);
		int i = 0;
		builder.append(parts[i++]);
		while(i<jokerIdnex) {
			builder.append('?');
			builder.append(parts[i++]);
		}
		builder.append(jokerSequence);
		builder.append(parts[i++]);
		while(i<parts.length) {
			builder.append('?');
			builder.append(parts[i++]);
		}
		return builder.toString();
	}
	
	private static String getJokerSequene(int jokerCount) {
		char[] jokerSeq = new char[3*jokerCount-2];
		for(int i=0; i<jokerCount-1; i++) {
			jokerSeq[3*i]   = '?';
			jokerSeq[3*i+1] = ',';
			jokerSeq[3*i+2] = ' ';
		}
		jokerSeq[3*jokerCount-3] = '?';
		return new String(jokerSeq);
	}
	
}
