package com.epam.news.common.dao.impl.util;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

public class Query {
	
	private static final Logger LOGGER = Logger.getLogger(Query.class);
	
	private String query;
	private PlainIndexProxy plainIndexProxy;
	
	public Query(String initQuery) {
		LOGGER.debug(String.format("New Query: %s", initQuery));
		this.query = initQuery;
		List<String> queryParams = parseParameters(initQuery);
		this.plainIndexProxy = new PlainIndexProxy(queryParams);
		LOGGER.debug(String.format("Result Query: %s", query));
	}
	
	public void setSubQuery(String queryName, Query subQuery) {
		LOGGER.debug(String.format("Set Sub Query #%s", queryName));
		LOGGER.debug(String.format("Sub Query: %s", subQuery));
		LOGGER.debug(String.format("Before: %s", query));
		plainIndexProxy.replaceParameter(queryName, subQuery.plainIndexProxy);
		String replacement = "(" + subQuery + ")";
		replace(queryName, replacement);
		LOGGER.debug(String.format("After:  %s", query));
	}
	
	public void setString(String paramName, Object paramValue) {
		LOGGER.debug(String.format("Set String #%s: %s", paramName, paramValue));
		plainIndexProxy.removeParameter(paramName);
		replace(paramName, paramValue.toString());
		LOGGER.debug(String.format("Result Query: %s", query));
	}
	
	public void setParameterList(String paramName, int listSize) {
		LOGGER.debug(String.format("Set Parameter List #%s: %d", paramName, listSize));
		plainIndexProxy.setParameterSize(paramName, listSize);
		StringBuilder listBuilder = new StringBuilder(3*listSize-2).append('(');
		for(int i=0; i<listSize-1; i++) {
			listBuilder.append("?, ");
		}
		listBuilder.append("?)");
		replace(paramName, listBuilder.toString());
		LOGGER.debug(String.format("Result Query: %s", query));
	}
	
	public void setParameter(String paramName) {
		LOGGER.debug(String.format("Set Parameter #%s.", paramName));
		plainIndexProxy.setParameterSize(paramName, 1);
		replace(paramName, "?");
		LOGGER.debug(String.format("Result Query: %s", query));
	}
	
	private void replace(String param, String value) {
		String regexp = getParamRegExp(param);
		LOGGER.debug("REGEXP = " + regexp);
		query = Pattern.compile(regexp).matcher(query).replaceAll(value);
	}
	
	private String getParamRegExp(String paramName) {
		return "\\$\\{" + paramName + "\\}";
	}
	
	public String getQuery() {
		return query;
	}
	
	public IndexProxy getIndexProxy() {
		return plainIndexProxy.toIndexProxy();
	}
	
	@Override
	public String toString() {
		return query;
	}
	
	private static List<String> parseParameters(String query) {
		LOGGER.debug("Query Parse Parameters ...");
		List<String> paramList = new LinkedList<>();
		Matcher matcher = Pattern.compile("\\$\\{(\\w+)\\}").matcher(query);
		while(matcher.find()) {
			paramList.add(matcher.group(1));
			LOGGER.debug(String.format("Found Parameter: %s", matcher.group(1)));
		}
		return paramList;
	}
	
}