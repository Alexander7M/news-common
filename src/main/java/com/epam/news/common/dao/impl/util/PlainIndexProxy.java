package com.epam.news.common.dao.impl.util;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class PlainIndexProxy {
	
	private static class Parameter {
		private String name;
		private int size;
		Parameter(String name) {
			this.name = name;
			this.size = 0;
		}
	}
	
	private List<Parameter> params = new LinkedList<Parameter>();
	
	public PlainIndexProxy(Iterable<String> paramNames) {
		for(String paramName : paramNames) {
			params.add(new Parameter(paramName));
		}
	}
	
	public boolean replaceParameter(String param, PlainIndexProxy anotherPlainTextProxy) {
		ListIterator<Parameter> it = params.listIterator();
		while(it.hasNext()) {
			if(it.next().name.equalsIgnoreCase(param)) {
				it.remove();
				for(Parameter newParam : anotherPlainTextProxy.params) {
					it.add(newParam);
				}
				return true;
			}
		}
		return false;
	}
		
//	public boolean replaceParameter(String param, Iterable<String> newParams) {
//		ListIterator<Parameter> it = params.listIterator();
//		while(it.hasNext()) {
//			if(it.next().name.equalsIgnoreCase(param)) {
//				it.remove();
//				for(String newParam : newParams) {
//					it.add(new Parameter(newParam));
//				}
//				return true;
//			}
//		}
//		return false;
//	}
	
	public boolean removeParameter(String param) {
		ListIterator<Parameter> it = params.listIterator();
		while(it.hasNext()) {
			if(it.next().name.equalsIgnoreCase(param)) {
				it.remove();
				return true;
			}
		}
		return false;
	}
	
	public boolean setParameterSize(String paramName, Integer newParamSize) {
		ListIterator<Parameter> it = params.listIterator();
		while(it.hasNext()) {
			Parameter param = it.next();
			if(param.name.equalsIgnoreCase(paramName)) {
				param.size = newParamSize;
				return true;
			}
		}
		return false;
	}
	
	public IndexProxy toIndexProxy() {
		IndexProxy targetProxy = new IndexProxy();
		int paramIndex = 1;
		for(Parameter param : params) {
			if (param.size > 0) {
				targetProxy.setIndex(param.name, paramIndex);
				paramIndex += param.size;
			}
		}
		return targetProxy;
	}
	
}
