package com.epam.news.common.dao;

import java.util.List;

import com.epam.news.common.dao.exception.DataAccessException;
import com.epam.news.common.entity.Tag;

public interface TagDao extends BatchCrudDao<Long, Tag> {
	
	/*
	 * every SELECT method of this class should return tags if they exist,
	 * otherwise he sould create tags and return them. 
	 */
	Tag selectByName(String tagName) throws DataAccessException;
	List<Tag> selectByNames(List<String> tagName) throws DataAccessException;
	
}