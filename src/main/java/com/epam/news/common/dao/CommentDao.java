package com.epam.news.common.dao;

import com.epam.news.common.entity.Comment;

public interface CommentDao extends BatchCrudDao<Long, Comment> {
	
}
