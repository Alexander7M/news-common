package com.epam.news.common.dao.impl;

import static com.epam.news.common.dao.impl.util.SQLQueryBuilder.expandJokerToArray;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.epam.news.common.dao.CommentDao;
import com.epam.news.common.dao.exception.DataAccessException;
import com.epam.news.common.dao.impl.util.IndexedStatementWriter;
import com.epam.news.common.dao.impl.util.TableMapper;
import com.epam.news.common.entity.Comment;

public class CommentDaoOracleImpl extends TransactionalDao implements CommentDao {

	private static final String ID_COLUMN = "COMMENT_ID";
	private static final String NEWS_ID_COLUMN = "NEWS_ID";
	private static final String TEXT_COLUMN = "COMMENT_TEXT";
	private static final String CREATION_DATE_COLUMN = "CREATION_DATE";
	
	public static class CommentTableMapper implements TableMapper<Comment> {	
		@Override
		public Comment mapRow(ResultSet resultSet) throws SQLException {
			Comment comment = new Comment();
			comment.setId(getLong(resultSet, ID_COLUMN));
			comment.setNewsId(getLong(resultSet, NEWS_ID_COLUMN));
			comment.setText(getString(resultSet, TEXT_COLUMN));
			comment.setCreationDate(getInstant(resultSet, CREATION_DATE_COLUMN));
			return comment;
		}
	}
	
	private IndexedStatementWriter indexedStatementWriter = new IndexedStatementWriter();
	private TableMapper<Comment> commentReader = new CommentTableMapper();
	private TableMapper<Long> idReader = (resultSet) -> {return resultSet.getLong(1);};
	
	private static final String SELECT_ALL_COMMENTS_SQL_QUERY = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM COMMENTS";
	private static final String SELECT_COMMENT_BY_KEY_SQL_QUERY = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM COMMENTS WHERE COMMENT_ID = ?";
	private static final String INSERT_COMMENT_SQL_QUERY = "INSERT INTO COMMENTS(NEWS_ID, COMMENT_TEXT, CREATION_DATE) VALUES(?, ?, ?)";
	private static final String UPDATE_COMMENT_SQL_QUERY = "UPDATE COMMENTS SET NEWS_ID = ?, COMMENT_TEXT = ?, CREATION_DATE = ? WHERE COMMENT_ID = ?";
	private static final String DELETE_COMMENT_SQL_QUERY = "DELETE FROM COMMENTS WHERE COMMENT_ID = ?";
	private static final String SELECT_COMMENTS_BY_KEYS_SQL_QUERY = 
			"SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE " + 
			"FROM COMMENTS " + 
			"WHERE COMMENT_ID IN (?)";	
	
	@Override
	public List<Comment> selectAllWithCommentCountSorting() throws DataAccessException {
		Connection connection = getConnection();
		try (Statement statement = connection.createStatement()) {
			ResultSet resultSet = statement.executeQuery(SELECT_ALL_COMMENTS_SQL_QUERY);
			return commentReader.mapTable(resultSet);
		}
		catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
	
	@Override
	public Comment selectByKey(Long id) throws DataAccessException {
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(SELECT_COMMENT_BY_KEY_SQL_QUERY)) {
			indexedStatementWriter.write(statement, 1, id);
			ResultSet resultSet = statement.executeQuery();
			return commentReader.mapFirstRow(resultSet);
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
		
	@Override
	public List<Comment> selectByKeys(List<Long> idList) throws DataAccessException {
		if(idList.size() == 0) {
			return new LinkedList<Comment>();
		}
		Connection connection = getConnection();
		String preparedQuery = expandJokerToArray(SELECT_COMMENTS_BY_KEYS_SQL_QUERY, 1, idList.size());
		try (PreparedStatement statement = connection.prepareStatement(preparedQuery)) {
			indexedStatementWriter.write(statement, 1, idList);
			ResultSet resultSet = statement.executeQuery();
			return commentReader.mapTable(resultSet);
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
	
	@Override
	public Long create(Comment comment) throws DataAccessException {
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(INSERT_COMMENT_SQL_QUERY, new String[]{"COMMENT_ID"})) {
			indexedStatementWriter.write(statement, 1, comment.getNewsId());
			indexedStatementWriter.write(statement, 2, comment.getText());
			indexedStatementWriter.write(statement, 3, comment.getCreationDate());
			statement.executeUpdate();
			ResultSet resultSet = statement.getGeneratedKeys();
			return idReader.mapFirstRow(resultSet);
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
	
	/*
	 * Unfortunetly, Oracle jdbc driver has next limitation:
	 * "You cannot combine auto-generated keys with batch update."
	 * hence, for multi update we have to use for cycle.
	 * @see http://docs.oracle.com/cd/B28359_01/java.111/b31224/jdbcvers.htm#CHDEGDHJ
	 */
	@Override
	public List<Long> create(List<Comment> commentList) throws DataAccessException {
		if (commentList.size() == 0) {
			return new LinkedList<Long>();
		} 
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(INSERT_COMMENT_SQL_QUERY, new String[] { "COMMENT_ID"})) {
			ArrayList<Long> idList = new ArrayList<Long>(commentList.size());
			for(Comment comment : commentList) {
				indexedStatementWriter.write(statement, 1, comment.getNewsId());
				indexedStatementWriter.write(statement, 2, comment.getText());
				indexedStatementWriter.write(statement, 3, comment.getCreationDate());
				statement.executeUpdate();
				ResultSet keySet = statement.getGeneratedKeys();
				Long generatedId = idReader.mapFirstRow(keySet);
				idList.add(generatedId);
				keySet.close();
			}
			return idList;
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
	
	@Override
	public void update(Comment comment) throws DataAccessException {
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(UPDATE_COMMENT_SQL_QUERY)) {
			indexedStatementWriter.write(statement, 1, comment.getNewsId());
			indexedStatementWriter.write(statement, 2, comment.getText());
			indexedStatementWriter.write(statement, 3, comment.getCreationDate());
			indexedStatementWriter.write(statement, 4, comment.getId());
			statement.executeUpdate();
		}
		catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}

	@Override
	public void update(List<Comment> commentList) throws DataAccessException {
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(UPDATE_COMMENT_SQL_QUERY)) {
			for(Comment comment : commentList) {
				indexedStatementWriter.write(statement, 1, comment.getNewsId());
				indexedStatementWriter.write(statement, 2, comment.getText());
				indexedStatementWriter.write(statement, 3, comment.getCreationDate());
				indexedStatementWriter.write(statement, 4, comment.getId());
				statement.addBatch();
			}
			statement.executeBatch();
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}

	@Override
	public void delete(Long id) throws DataAccessException {
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(DELETE_COMMENT_SQL_QUERY)) {
			indexedStatementWriter.write(statement, 1, id);
			statement.executeUpdate();
		}catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
	
	@Override
	public void delete(List<Long> idList) throws DataAccessException {
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(DELETE_COMMENT_SQL_QUERY)) {
			for(Long id : idList) {
				indexedStatementWriter.write(statement, 1, id);
				statement.addBatch();
			}
			statement.executeBatch();
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}

}
