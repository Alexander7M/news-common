package com.epam.news.common.dao;

import java.util.List;

import com.epam.news.common.dao.exception.DataAccessException;

public interface CrudDao<K, E> {
	
	List<E> selectAllWithCommentCountSorting() throws DataAccessException;
	E selectByKey(K key) throws DataAccessException;
	K create(E entity) throws DataAccessException;
	void update(E entity) throws DataAccessException;
	void delete(K key) throws DataAccessException;
	
}
