package com.epam.news.common.dao;

import java.util.List;

import com.epam.news.common.dao.exception.DataAccessException;
import com.epam.news.common.entity.Author;

public interface AuthorDao extends CrudDao<Long, Author> {

	List<Author> selectByName(String authorName) throws DataAccessException;
	boolean updateExpiredField(Author author) throws DataAccessException;
	boolean updateNameField(Author author) throws DataAccessException;
	
}
