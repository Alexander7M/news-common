package com.epam.news.common.dao.impl;

import java.sql.Connection;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

public class TransactionalDao {

	private DataSource dataSource; 
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	public Connection getConnection() {
		return DataSourceUtils.getConnection(dataSource);
	}
	
	public void releaseConnection(Connection connection) {
		DataSourceUtils.releaseConnection(connection, dataSource);
	}
	
}
