package com.epam.news.common.dao;

import java.util.List;

import com.epam.news.common.dao.exception.DataAccessException;

public interface BatchCrudDao<K, E> extends CrudDao<K, E> {

	List<E> selectByKeys(List<K> keys) throws DataAccessException;
	/*
	 * Unfortunetly, Oracle jdbc driver has next limitation:
	 * "You cannot combine auto-generated keys with batch update."
	 * hence, for multi update we have to use for cycle.
	 * @see http://docs.oracle.com/cd/B28359_01/java.111/b31224/jdbcvers.htm#CHDEGDHJ
	 */
	List<K> create(List<E> enties) throws DataAccessException;
	void update(List<E> entity) throws DataAccessException;
	void delete(List<K> keys) throws DataAccessException;
	
}
