package com.epam.news.common.dao.impl;

import static com.epam.news.common.dao.impl.util.SQLQueryBuilder.expandJokerToArray;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.epam.news.common.dao.NewsDao;
import com.epam.news.common.dao.exception.DataAccessException;
import com.epam.news.common.dao.impl.util.IndexedStatementWriter;
import com.epam.news.common.dao.impl.util.NamedStatementWriter;
import com.epam.news.common.dao.impl.util.Query;
import com.epam.news.common.dao.impl.util.TableMapper;
import com.epam.news.common.dto.NewsFilter;
import com.epam.news.common.dto.NewsSorting;
import com.epam.news.common.dto.Pagination;
import com.epam.news.common.entity.News;

@Repository
public class NewsDaoOracleImpl extends TransactionalDao implements NewsDao {
	
	private static final Logger LOGGER = Logger.getLogger(NewsDaoOracleImpl.class);
	
	private static final String ID_COLUMN = "NEWS_ID";
	private static final String TITLE_COLUMN = "TITLE";
	private static final String SHORT_TEXT_COLUMN = "SHORT_TEXT";
	private static final String FULL_TEXT_COLUMN = "FULL_TEXT";
	private static final String CREATION_DATE_COLUMN = "CREATION_DATE";
	private static final String MODIFICATION_DATE_COLUMN = "MODIFICATION_DATE";
	
	public class NewsHeaderTableMapper implements TableMapper<News> {
		@Override
		public News mapRow(ResultSet resultSet) throws SQLException {
			News newsHeader = new News();
			newsHeader.setId(getLong(resultSet, ID_COLUMN));
			newsHeader.setTitle(getString(resultSet, TITLE_COLUMN));
			newsHeader.setShortText(getString(resultSet, SHORT_TEXT_COLUMN));
			newsHeader.setCreationDate(getInstant(resultSet, CREATION_DATE_COLUMN));
			newsHeader.setModificationDate(getDate(resultSet, MODIFICATION_DATE_COLUMN));
			return newsHeader;
		}		
	}
	
	public final class NewsRowMapper extends NewsHeaderTableMapper {	
		@Override
		public News mapRow(ResultSet resultSet, int rowNum) throws SQLException {
			News news = super.mapRow(resultSet, rowNum);
			news.setFullText(getString(resultSet, FULL_TEXT_COLUMN));
			return news;
		}	
	}
	
	private final IndexedStatementWriter indexedStatementWriter = new IndexedStatementWriter();
	private final TableMapper<News> newsReader = new NewsRowMapper();
	private final TableMapper<News> newsHeaderReader = new NewsHeaderTableMapper();
	private final TableMapper<Long> idReader = (resultSet) -> {return resultSet.getLong(1);};
	
	/*============================ FILTER ============================*/
	
	private static final String SELECT_NEWS_THROUGH_AUTHOR_FILTER_SQL_QUERY = 
		" SELECT " +
			" NEWS_TABLE.* " + 
		" FROM ${NEWS_TABLE} NEWS_TABLE INNER JOIN NEWS_AUTHOR ON NEWS_TABLE.NEWS_ID = NEWS_AUTHOR.NEWS_ID " + 
		" WHERE NEWS_AUTHOR.AUTHOR_ID = ${AUTHOR_ID}";
	
	private static final String SELECT_NEWS_THROUGH_TAG_FILTER_SQL_QUERY = 
		" SELECT " +
			" NEWS_TABLE.* " +
		" FROM ${NEWS_TABLE} NEWS_TABLE INNER JOIN ${FILTERED_NEWS_ID} FILTERED_NEWS_ID " + 
		" ON NEWS_TABLE.NEWS_ID = FILTERED_NEWS_ID.NEWS_ID ";
	
	private static final String SELECT_NEWS_ID_THROUGH_TAG_FILTER_SQL_QUERY = 
		" SELECT NEWS_ID " +
		" FROM ( " +
			" SELECT NEWS_ID, COUNT(*) AS TAG_NUMBER " +
			" FROM NEWS_TAG " +
			" WHERE TAG_ID IN ${TAG_ID_LIST} " + 
			" GROUP BY NEWS_ID " +
		" ) TAGED_NEWS " +
		" WHERE TAG_NUMBER >= ${MIN_TAG_NUMBER} ";
	
	/*============================ SORTING ============================*/
	
	private static final String SELECT_NEWS_ORDER_BY_COMMENT_COUNT_SQL_QUERY =
		" SELECT " + 
			" NEWS_TABLE.*, " + 
			" COALESCE(COUNTED_NEWS.COMMENT_COUNT, 0) AS COMMENT_COUNT " +
		" FROM ${NEWS_TABLE} NEWS_TABLE " +
		" LEFT JOIN (SELECT NEWS_ID, COUNT(*) AS COMMENT_COUNT FROM COMMENTS GROUP BY NEWS_ID) COUNTED_NEWS " +
		" ON NEWS_TABLE.NEWS_ID = COUNTED_NEWS.NEWS_ID " +
		" ORDER BY COMMENT_COUNT ${ORDER}";
	
	private static final String SELECT_NEWS_ORDER_BY_CREATION_DATE_SQL_QUERY = 
		" SELECT * FROM ${NEWS_TABLE} NEWS_TABLE ORDER BY CREATION_DATE ${ORDER}";
	
	/*============================ PAGINATION ============================*/
	
	private static final String SELECT_NEWS_WITH_PAGINATION_SQL_QUERY = 
		" SELECT * FROM ${NEWS_TABLE} NEWS_TABLE WHERE ROWNUM BETWEEN ${FIRST_ITEM} AND ${LAST_ITEM} ";
	
	/*============================ PLAIN SELECTION ============================*/
	
	private static final String SELECT_ALL_NEWS_WITH_COMMENT_COUNT_ORDER_SQL_QUERY =
		" SELECT " + 
			" NEWS.*, " + 
			" COALESCE(COUNTED_NEWS.COMMENT_COUNT, 0) AS COMMENT_COUNT " +
		" FROM NEWS " +
		" LEFT JOIN (SELECT NEWS_ID, COUNT(*) AS COMMENT_COUNT FROM COMMENTS GROUP BY NEWS_ID) COUNTED_NEWS " +
		" ON NEWS.NEWS_ID = COUNTED_NEWS.NEWS_ID " +
		" ORDER BY COMMENT_COUNT DESC";
	
	private static final String SELECT_NEWS_BY_ID_SQL_QUERY  = 
		" SELECT " + 
			" NEWS_ID, " + 
			" TITLE, " + 
			" SHORT_TEXT, " + 
			" FULL_TEXT, " + 
			" CREATION_DATE, " + 
			" MODIFICATION_DATE " + 
		" FROM NEWS " + 
		" WHERE NEWS_ID = ? ";
	
	private static final String SELECT_NEWS_BY_AUTHOR_ID_SQL_QUERY  = 
		"SELECT " + 
			"NEWS.NEWS_ID AS NEWS_ID, " + 
			"NEWS.TITLE AS TITLE, " + 
			"NEWS.SHORT_TEXT AS SHORT_TEXT, " + 
			"NEWS.CREATION_DATE AS CREATION_DATE, " + 
			"NEWS.MODIFICATION_DATE AS MODIFICATION_DATE " + 
		"FROM NEWS INNER JOIN NEWS_AUTHOR ON NEWS.NEWS_ID = NEWS_AUTHOR.NEWS_ID " + 
		"WHERE NEWS_AUTHOR.AUTHOR_ID = ?";
	
	private static final String SELECT_ALL_HEADERS_NEWS_SQL_QUERY = 
			"SELECT NEWS_ID, TITLE, SHORT_TEXT, CREATION_DATE, MODIFICATION_DATE FROM NEWS";
		
	private static final String SELECT_NEWS_BY_TAGS_SQL_QUERY = 
		" SELECT " +
			" NEWS.* " + 
		" FROM NEWS " + 
		" INNER JOIN (SELECT NEWS_ID, COUNT(TAG_ID) AS TAG_COUNT FROM NEWS_TAG WHERE TAG_ID IN (?) GROUP BY NEWS_ID) COUNT_TABLE " +
		" ON NEWS.NEWS_ID = COUNT_TABLE.NEWS_ID " +
		" ORDER BY TAG_COUNT DESC";
		
	/*============================ UPDATE ============================*/
	
	private static final String UPDATE_NEWS_BY_ID_SQL_QUERY = 
		"UPDATE NEWS SET " + 
			" TITLE = ?, " + 
			" SHORT_TEXT = ?, " + 
			" FULL_TEXT = ?, " + 
			" CREATION_DATE = ?, " + 
			" MODIFICATION_DATE = ? " + 
		" WHERE NEWS_ID = ?";
	
	/*============================ INSERTION ============================*/
	
	private static final String INSERT_NEWS_SQL_QUERY = 
		" INSERT INTO " +
		" NEWS (TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) " + 
		" VALUES (?, ?, ?, ?, ?)";
	
	private static final String INSERT_NEWS_AUTHOR_SQL_QUERY = 
		"INSERT INTO NEWS_AUTHOR(NEWS_ID, AUTHOR_ID) VALUES(?, ?)";
	
	private static final String INSERT_NEWS_TAG_SQL_QUERY = 
		"INSERT INTO NEWS_TAG(NEWS_ID, TAG_ID) VALUES(?, ?)";
	
	/*============================ REMOVAL ============================*/
	
	private static final String DELETE_NEWS_BY_ID_SQL_QUERY = 
		" DELETE FROM NEWS WHERE NEWS_ID = ? ";
	
	private static final String DELETE_NEWS_AUTROR_SQL_QUERY = 
		"DELETE FROM NEWS_AUTHOR WHERE NEWS_ID = ? AND AUTHOR_ID = ?";
	
	private static final String DELETE_NEWS_TAG_SQL_QUERY = 
		"DELETE FROM NEWS_TAG WHERE NEWS_ID = ? AND TAG_ID = ?";
	
	@Override
	public Long create(News news) throws DataAccessException {
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(INSERT_NEWS_SQL_QUERY, new String[] { "NEWS_ID" })) {
			indexedStatementWriter.write(statement, 1, news.getTitle());
			indexedStatementWriter.write(statement, 2, news.getShortText());
			indexedStatementWriter.write(statement, 3, news.getFullText());
			indexedStatementWriter.write(statement, 4, news.getCreationDate());
			indexedStatementWriter.write(statement, 5, news.getModificationDate());
			statement.executeUpdate();
			ResultSet keySet = statement.getGeneratedKeys();
			return idReader.mapFirstRow(keySet);
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
	
	@Override
	public List<News> selectAllWithCommentCountSorting() throws DataAccessException {
		Connection connection = getConnection();
		try (Statement statement = connection.createStatement()) {
			ResultSet resultSet = statement.executeQuery(SELECT_ALL_NEWS_WITH_COMMENT_COUNT_ORDER_SQL_QUERY);
			return newsReader.mapTable(resultSet);
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
		
	}
		
	@Override
	public News selectByKey(Long key) throws DataAccessException {
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(SELECT_NEWS_BY_ID_SQL_QUERY)) {
			statement.setString(1, key.toString());
			ResultSet resultSet = statement.executeQuery();
			return newsReader.mapFirstRow(resultSet);
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
	
	@Override
	public void delete(Long key) throws DataAccessException {
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(DELETE_NEWS_BY_ID_SQL_QUERY)) {
			statement.setString(1, key.toString());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
	
	@Override
	public void update(News news) throws DataAccessException {
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(UPDATE_NEWS_BY_ID_SQL_QUERY)) {
			indexedStatementWriter.write(statement, 1, news.getTitle());
			indexedStatementWriter.write(statement, 2, news.getShortText());
			indexedStatementWriter.write(statement, 3, news.getFullText());
			indexedStatementWriter.write(statement, 4, news.getCreationDate());
			indexedStatementWriter.write(statement, 5, news.getModificationDate());
			statement.setString(6, news.getId().toString());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
	
	@Override
	public List<News> selectByAuthor(Long authorId) throws DataAccessException {
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(SELECT_NEWS_BY_AUTHOR_ID_SQL_QUERY)) {
			statement.setString(1, authorId.toString());
			ResultSet resultSet = statement.executeQuery();
			return newsHeaderReader.mapTable(resultSet);
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
		
	@Override
	public void addNewsAuthor(Long newsId, Long authorId) throws DataAccessException {
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(INSERT_NEWS_AUTHOR_SQL_QUERY)) {
			statement.setString(1, newsId.toString());
			statement.setString(2, authorId.toString());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
		
	@Override
	public void deleteNewsAuthor(Long newsId, Long authorId) throws DataAccessException {
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(DELETE_NEWS_AUTROR_SQL_QUERY)) {
			statement.setString(1, newsId.toString());
			statement.setString(2, authorId.toString());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
	
	@Override
	public List<News> selectByTags(List<Long> tagsIds) throws DataAccessException {
		Connection connection = getConnection();
		String result_sql_query = expandJokerToArray(SELECT_NEWS_BY_TAGS_SQL_QUERY, 1, tagsIds.size());
		try (PreparedStatement statement = connection.prepareStatement(result_sql_query)) {
			indexedStatementWriter.write(statement, 1, tagsIds);
			ResultSet resultSet = statement.executeQuery();
			return newsHeaderReader.mapTable(resultSet);
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
		
	@Override
	public void addNewsTag(Long newsId, Long tagId) throws DataAccessException {
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(INSERT_NEWS_TAG_SQL_QUERY)) {
			statement.setString(1, newsId.toString());
			statement.setString(2, tagId.toString());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}

	@Override
	public void addNewsTags(Long newsId, List<Long> tagIds) throws DataAccessException {
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(INSERT_NEWS_TAG_SQL_QUERY)) {
			for (Long tagId : tagIds) {
				statement.setString(1, newsId.toString());
				statement.setString(2, tagId.toString());
				statement.addBatch();
			}
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
	
	@Override
	public void deleteNewsTag(Long newsId, Long tagId) throws DataAccessException {
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(DELETE_NEWS_TAG_SQL_QUERY)) {
			statement.setString(1, newsId.toString());
			statement.setString(2, tagId.toString());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}

	@Override
	public void deleteNewsTags(Long newsId, List<Long> tagsId) throws DataAccessException {
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(DELETE_NEWS_TAG_SQL_QUERY)) {
			for(Long tagId : tagsId) {
				statement.setString(1, newsId.toString());
				statement.setString(2, tagId.toString());
				statement.addBatch();
			}
			statement.executeBatch();
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
	
	@Override
	public List<News> selectAllHeaders() throws DataAccessException {
		Connection connection = getConnection();
		try (Statement statement = connection.createStatement()) {
			ResultSet resultSet = statement.executeQuery(SELECT_ALL_HEADERS_NEWS_SQL_QUERY);
			return newsHeaderReader.mapTable(resultSet);
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}

	@Override
	public List<News> selectHeaders(NewsFilter filter, NewsSorting sorting, Pagination pagination) throws DataAccessException {
		Connection connection = getConnection();
		Query authorFilteringQuery = createAuthorFilteringQuery(filter, null);
		Query filteringQuery = createTagFilteringQuery(filter, authorFilteringQuery);
		Query sortingQueryWithFilter = createSortingQuery(sorting, filteringQuery);
		Query paginationQueryWithSortingAndFilter = createPaginationQuery(pagination, sortingQueryWithFilter);
		Query finalQuery = paginationQueryWithSortingAndFilter;
		LOGGER.debug("Final Query:\n" + finalQuery.getQuery());
		LOGGER.debug("Final Index:\n" + finalQuery.getIndexProxy());
		try (PreparedStatement statement = connection.prepareStatement(finalQuery.toString())) {
			NamedStatementWriter statementWriter = new NamedStatementWriter(statement, finalQuery.getIndexProxy());
			if (filter.getRequiredAuthor() != null) {
				statementWriter.setLongParameter("AUTHOR_ID", filter.getRequiredAuthor());
			}
			if (filter.hasTagConstraint()) {
				statementWriter.setIterableParameter("TAG_ID_LIST", filter.getSpecifiedTags());
			}
			statementWriter.setIntegerParameter("FIRST_ITEM", pagination.getFirstIndex());
			statementWriter.setIntegerParameter("LAST_ITEM", pagination.getLastIndex());
			ResultSet result = statement.executeQuery();
			return newsHeaderReader.mapTable(result);
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
	
	private Query createPaginationQuery(Pagination pagination, Query subQuery) {
		Query paginationQuery = new Query(SELECT_NEWS_WITH_PAGINATION_SQL_QUERY);
		paginationQuery.setParameter("FIRST_ITEM");
		paginationQuery.setParameter("LAST_ITEM");
		if (subQuery != null) {
			paginationQuery.setSubQuery("NEWS_TABLE", subQuery);
		} else {
			Query selectAllQuery = new Query("SELECT * FROM NEWS");
			paginationQuery.setSubQuery("NEWS_TABLE", selectAllQuery);
		}
		return paginationQuery;
	}
	
	private Query createSortingQuery(NewsSorting sorting, Query subQuery) {
		Query sortingQuery;
		switch(sorting.type) {
		case WITHOUT_ANY_SORTING:
			return subQuery;
		case COMMENT_NUMBER_SORTING:
			sortingQuery = new Query(SELECT_NEWS_ORDER_BY_COMMENT_COUNT_SQL_QUERY);
			break;
		case CREATION_DATE_SORTING:
			sortingQuery = new Query(SELECT_NEWS_ORDER_BY_CREATION_DATE_SQL_QUERY);
			break;
		default:
			throw new EnumConstantNotPresentException(NewsSorting.SortingType.class, sorting.type.name());
		}
		sortingQuery.setString("ORDER", sorting.order);
		if (subQuery != null) {
			sortingQuery.setSubQuery("NEWS_TABLE", subQuery);
		} else {
			sortingQuery.setString("NEWS_TABLE", "NEWS");
		}
		return sortingQuery;
	}
	
	public Query createAuthorFilteringQuery(NewsFilter filter, Query subQuery) {
		if (filter.getRequiredAuthor() == null) {
			return subQuery;
		}
		Query authorFilteringQuery = new Query(SELECT_NEWS_THROUGH_AUTHOR_FILTER_SQL_QUERY);
		authorFilteringQuery.setParameter("AUTHOR_ID");
		if(subQuery != null) {
			authorFilteringQuery.setSubQuery("NEWS_TABLE", subQuery);
		} else {
			authorFilteringQuery.setString("NEWS_TABLE", "NEWS");
		}
		return authorFilteringQuery;
	}
	
	public Query createTagFilteringQuery(NewsFilter filter, Query subQuery) {
		if(!filter.hasTagConstraint()) {
			return subQuery;
		}
		Query tagFilteringQuery = new Query(SELECT_NEWS_THROUGH_TAG_FILTER_SQL_QUERY);
		Query newsIdTagFilteringQuery = createNewsIdTagFilteringQuery(filter);
		tagFilteringQuery.setSubQuery("FILTERED_NEWS_ID", newsIdTagFilteringQuery);
		if(subQuery != null) {
			tagFilteringQuery.setSubQuery("NEWS_TABLE", subQuery);
		} else {
			tagFilteringQuery.setString("NEWS_TABLE", "NEWS");
		}
		return tagFilteringQuery;
	}
	
	public Query createNewsIdTagFilteringQuery(NewsFilter filter) {
		Query newsIdTagFilteringQuery = new Query(SELECT_NEWS_ID_THROUGH_TAG_FILTER_SQL_QUERY);
		newsIdTagFilteringQuery.setString("MIN_TAG_NUMBER", filter.getSpecifiedTags().size());
		newsIdTagFilteringQuery.setParameterList("TAG_ID_LIST", filter.getSpecifiedTags().size());
		return newsIdTagFilteringQuery;
	}
	
}
