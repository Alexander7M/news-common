package com.epam.news.common.dao.impl;

import static com.epam.news.common.dao.impl.util.SQLQueryBuilder.expandJokerToArray;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.epam.news.common.dao.TagDao;
import com.epam.news.common.dao.exception.DataAccessException;
import com.epam.news.common.dao.impl.util.IndexedStatementWriter;
import com.epam.news.common.dao.impl.util.TableMapper;
import com.epam.news.common.entity.Tag;

public class TagDaoOracleImpl extends TransactionalDao implements TagDao {
	
	private static final String TAG_ID_COLUMN = "TAG_ID";
	private static final String TAG_NAME_COLUMN = "TAG_NAME";
	
	public static class TagTableMapper implements TableMapper<Tag> {
		@Override
		public Tag mapRow(ResultSet resultSet) throws SQLException {
			Long id = getLong(resultSet, TAG_ID_COLUMN);
			String name = getString(resultSet, TAG_NAME_COLUMN);
			return new Tag(id, name);
		}
	}
	
	private final IndexedStatementWriter tagWriter = new IndexedStatementWriter();
	private final TableMapper<Tag> tagReader = new TagTableMapper();
	private final TableMapper<Long> idReader = (resultSet) -> {return resultSet.getLong(1);};
	
	private static final String SELECT_ALL_TAGS_SQL_QUERY = "SELECT TAG_ID, TAG_NAME FROM TAG";
	private static final String SELECT_TAG_BY_KEY_SQL_QUERY = "SELECT TAG_ID, TAG_NAME FROM TAG WHERE TAG_ID = ?";
	private static final String SELECT_TAG_BY_NAME_SQL_QUERY = "SELECT TAG_ID, TAG_NAME FROM TAG WHERE TAG_NAME = ?";
	private static final String INSERT_TAG_SQL_QUERY = "INSERT INTO TAG(TAG_NAME) VALUES(?)";
	private static final String UPDATE_TAG_SQL_QUERY = "UPDATE TAG SET TAG_NAME = ? WHERE TAG_ID = ?";
	private static final String DELETE_TAG_SQL_QUERY = "DELETE FROM TAG WHERE TAG_ID = ?";
	private static final String SELECT_TAGS_BY_KEYS_SQL_QUERY = "SELECT TAG_ID, TAG_NAME FROM TAG WHERE TAG_ID IN (?)";
	private static final String SELECT_TAGS_BY_NAMES_SQL_QUERY = "SELECT TAG_ID, TAG_NAME FROM TAG WHERE TAG_NAME IN (?)"; 
	
	@Override
	public List<Tag> selectAllWithCommentCountSorting() throws DataAccessException {
		Connection connection = getConnection();
		try (Statement statement = connection.createStatement()) {
			ResultSet resultSet = statement.executeQuery(SELECT_ALL_TAGS_SQL_QUERY);
			return tagReader.mapTable(resultSet);
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
	
	@Override
	public Tag selectByKey(Long id) throws DataAccessException {
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(SELECT_TAG_BY_KEY_SQL_QUERY)) {
			tagWriter.write(statement, 1, id);
			ResultSet resultSet = statement.executeQuery();
			return tagReader.mapFirstRow(resultSet);
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
	
	@Override
	public Tag selectByName(String tagName) throws DataAccessException {
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(SELECT_TAG_BY_NAME_SQL_QUERY)) {
			tagWriter.write(statement, 1, tagName);
			ResultSet resultSet = statement.executeQuery();
			return tagReader.mapFirstRow(resultSet);
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
		
	@Override
	public Long create(Tag tag) throws DataAccessException {
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(INSERT_TAG_SQL_QUERY, new String[] { "TAG_ID" })) {
			tagWriter.write(statement, 1, tag.getName());
			statement.executeUpdate();
			ResultSet keySet = statement.getGeneratedKeys();
			return idReader.mapFirstRow(keySet);
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
	
	@Override
	public void update(Tag tag) throws DataAccessException {
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(UPDATE_TAG_SQL_QUERY)) {
			tagWriter.write(statement, 1, tag.getName());
			tagWriter.write(statement, 2, tag.getId());
			statement.executeUpdate();
		}
		catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
	
	@Override
	public List<Tag> selectByKeys(List<Long> idList) throws DataAccessException {
		if(idList.size() == 0) {
			return new LinkedList<Tag>();
		}
		Connection connection = getConnection();
		String preparedQuery = expandJokerToArray(SELECT_TAGS_BY_KEYS_SQL_QUERY, 1, idList.size());
		try (PreparedStatement statement = connection.prepareStatement(preparedQuery)) {
			tagWriter.write(statement, 1, idList);
			ResultSet resultSet = statement.executeQuery();
			return tagReader.mapTable(resultSet);
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
	
	@Override
	public List<Tag> selectByNames(List<String> nameList) throws DataAccessException {
		if(nameList.size() == 0) {
			return new LinkedList<Tag>();
		}
		Connection connection = getConnection();
		String preparedQuery = expandJokerToArray(SELECT_TAGS_BY_NAMES_SQL_QUERY, 1, nameList.size());
		try (PreparedStatement statement = connection.prepareStatement(preparedQuery)) {
			tagWriter.write(statement, 1, nameList);
			ResultSet resultSet = statement.executeQuery();
			return tagReader.mapTable(resultSet);
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
	
	/*
	 * Unfortunetly, Oracle jdbc driver has next limitation:
	 * "You cannot combine auto-generated keys with batch update."
	 * hence, for multi update we have to use for cycle.
	 * @see http://docs.oracle.com/cd/B28359_01/java.111/b31224/jdbcvers.htm#CHDEGDHJ
	 */
	@Override
	public List<Long> create(List<Tag> tags) throws DataAccessException {
		if (tags.size() == 0) {
			return new LinkedList<Long>();
		}
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(INSERT_TAG_SQL_QUERY, new String[] {"TAG_ID"})) {
			ArrayList<Long> idList = new ArrayList<Long>(tags.size());
			for(Tag tag : tags) {
				tagWriter.write(statement, 1, tag.getName());
				statement.executeUpdate();
				ResultSet keySet = statement.getGeneratedKeys();
				Long generatedId = idReader.mapFirstRow(keySet);
				idList.add(generatedId);
				keySet.close();
			}
			return idList;
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
	
	@Override
	public void update(List<Tag> tags) throws DataAccessException {
		Connection connection = getConnection();
			try (PreparedStatement statement = connection.prepareStatement(UPDATE_TAG_SQL_QUERY)) {
				for(Tag tag : tags) {
					tagWriter.write(statement, 1, tag.getName());
					tagWriter.write(statement, 2, tag.getId());
					statement.addBatch();
				}
				statement.executeBatch();
			}
		catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
	
	@Override
	public void delete(Long id) throws DataAccessException {
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(DELETE_TAG_SQL_QUERY)) {
			tagWriter.write(statement, 1, id);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
	
	@Override
	public void delete(List<Long> keys) throws DataAccessException {
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(DELETE_TAG_SQL_QUERY)) {
			for(Long id : keys) {
				tagWriter.write(statement, 1, id);
				statement.addBatch();
			}
			statement.executeBatch();
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			releaseConnection(connection);
		}
	}
	
}
