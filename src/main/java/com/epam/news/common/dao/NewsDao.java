package com.epam.news.common.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.epam.news.common.dao.exception.DataAccessException;
import com.epam.news.common.dto.NewsFilter;
import com.epam.news.common.dto.NewsSorting;
import com.epam.news.common.dto.Pagination;
import com.epam.news.common.entity.News;

@Repository
public interface NewsDao extends CrudDao<Long, News> {
	
	void addNewsAuthor(Long newsId, Long authorId) throws DataAccessException;
	void addNewsTags(Long newsId, List<Long> tagsId) throws DataAccessException;
	void addNewsTag(Long newsId, Long tagId) throws DataAccessException;
	void deleteNewsAuthor(Long newsId, Long authorId) throws DataAccessException;
	void deleteNewsTags(Long newsId, List<Long> tagIds) throws DataAccessException;
	void deleteNewsTag(Long newsId, Long tagId) throws DataAccessException;
	
	List<News> selectAllHeaders() throws DataAccessException;
	List<News> selectByAuthor(Long authorId) throws DataAccessException;
	List<News> selectByTags(List<Long> tagsIds) throws DataAccessException;
	
	List<News> selectHeaders(NewsFilter filter, NewsSorting sortingType, Pagination pagination) throws DataAccessException;
	
}
