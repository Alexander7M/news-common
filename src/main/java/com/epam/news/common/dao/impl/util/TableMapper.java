package com.epam.news.common.dao.impl.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

@FunctionalInterface
public interface TableMapper<E> extends RowMapper<E> {
	
	default E mapFirstRow(ResultSet resultSet) throws SQLException {
		return resultSet.next() ?  mapRow(resultSet, 0) : null;
	}
	
	default List<E> mapTable(ResultSet resultSet) throws SQLException {
		LinkedList<E> entities = new LinkedList<>();
		int rowNum = 0;
		while(resultSet.next()) {
			E justAnotherEntity = mapRow(resultSet, rowNum++);
			entities.add(justAnotherEntity);
		}
		return entities;
	}
	
	E mapRow(ResultSet resultSet) throws SQLException;
	
	default E mapRow(ResultSet resultSet, int rowNum) throws SQLException {
		return mapRow(resultSet);
	}
	
	default Instant getInstant(ResultSet resultSet, String columnName) throws SQLException {
		Timestamp timestamp = resultSet.getTimestamp(columnName);
		return timestamp != null ? timestamp.toInstant() : null;
	}

	default Instant getInstant(ResultSet resultSet, int columnIndex) throws SQLException {
		Timestamp timestamp = resultSet.getTimestamp(columnIndex);
		return timestamp != null ? timestamp.toInstant() : null;
	}
	
	default java.util.Date getDate(ResultSet resultSet, String columnName) throws SQLException {
		return resultSet.getTimestamp(columnName);
	}
	
	default java.util.Date getDate(ResultSet resultSet, int columnIndex) throws SQLException {
		return resultSet.getTimestamp(columnIndex);
	}
	
	default String getString(ResultSet resultSet, String columnName) throws SQLException {
		return resultSet.getString(columnName);
	}
	
	default String getString(ResultSet resultSet, int columnIndex) throws SQLException {
		return resultSet.getString(columnIndex);
	}
	
	default Long getLong(ResultSet resultSet, String columnName) throws SQLException {
		return resultSet.getLong(columnName);
	}
	
	default Long getLong(ResultSet resultSet, int columnIndex) throws SQLException {
		return resultSet.getLong(columnIndex);
	}
	
}