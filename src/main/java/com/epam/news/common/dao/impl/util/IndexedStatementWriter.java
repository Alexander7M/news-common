package com.epam.news.common.dao.impl.util;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Date;

public class IndexedStatementWriter {
	
	public IndexedStatementWriter() {
		
	}
	
	/*
	 * #TODO = WARN! This method use write(PreparedStatement, int, Object) method.
	 */
	public <T> void write(PreparedStatement statement, int index, Iterable<T> collection) throws SQLException {
		for (T element : collection) {
			write(statement, index++, element);
		}
	}
	
	public void write(PreparedStatement statement, int index, Object object) throws SQLException {
		statement.setObject(index, object);
	}
	
	public void write(PreparedStatement statement, int index, Long value) throws SQLException {
		statement.setLong(index, value);
	}
	
	public void write(PreparedStatement statement, int index, Integer value) throws SQLException {
		statement.setInt(index, value);
	}
	
	public void write(PreparedStatement statement, int index, String string) throws SQLException {
		statement.setString(index, string);
	}
	
	public void write(PreparedStatement statement, int index, Instant instant) throws SQLException {
		Timestamp timestamp = instant != null ? Timestamp.from(instant) : null;
		statement.setTimestamp(index, timestamp);
	}
	
	/*
	 * java.sql.Date dosen't represent time, only date.
	 * Hence, method setDate in class PreparedStatement is not applicable to set OffsetDateTime
	 * because he requires an java.sql.Date object.
	 * So, to save date with time we must use Timestamp.
	 * There is a link to Oracle below, where this question is discussing.
	 * @see <a href="http://www.oracle.com/technetwork/database/enterprise-edition/jdbc-faq-090281.html#08_01">What is going on with DATE and TIMESTAMP?</a>
	 */
	public void write(PreparedStatement statement, int index, Date date) throws SQLException {
		Instant instant = date.toInstant();
		Timestamp timestamp = Timestamp.from(instant);
		statement.setTimestamp(index, timestamp);
	}
	
}
