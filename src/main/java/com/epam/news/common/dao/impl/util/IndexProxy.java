package com.epam.news.common.dao.impl.util;

import static java.util.stream.Collectors.toList;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class IndexProxy {
	
	private Map<String, Integer> paramNameMap = new HashMap<>();

	public void setIndex(String paramName, int paramIndex) {
		paramNameMap.put(paramName, paramIndex);
	}
	
	public int getIndex(String paramName) {
		return paramNameMap.get(paramName);
	}
	
	@Override
	public String toString() {
		List<String> paramStringList = new LinkedList<String>();
		for(Map.Entry<String, Integer> parameter : paramNameMap.entrySet().
			stream().sorted((e1, e2) -> {return e1.getValue() - e2.getValue();}).collect(toList())) {
			paramStringList.add(String.format("{%d: %s}", parameter.getValue(), parameter.getKey()));
		}
		return "{" + String.join(", ", paramStringList) + "}";
	}
	
}