package com.epam.news.common.util;

public interface BiConverter<S, T> {

	T convertForward(S s);
	S convertBackward(T t);
	
}
