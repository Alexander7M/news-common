package com.epam.news.common.util;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

import com.epam.news.common.util.BiConverter;

@Deprecated
public class InstantToStringBiConverter implements BiConverter<Instant, String> {
	
	private static final String LOCAL_DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
	
	public static final InstantToStringBiConverter DEFAULT_CONVERTER = new InstantToStringBiConverter(LOCAL_DATE_TIME_PATTERN);
	
	private final DateTimeFormatter formatter;
	
	public InstantToStringBiConverter(String instantPattern) {
		this.formatter = DateTimeFormatter.ofPattern(instantPattern);
	}
	
	@Override
	public String convertForward(Instant instant) {
		String instantString = null;
		if (instant != null) {
			OffsetDateTime offsetDateTime = instant.atOffset(ZoneOffset.UTC);
			instantString = offsetDateTime.format(formatter);
		}
		return instantString;
	}

	@Override
	public Instant convertBackward(String localDateTimeString) {
		if(localDateTimeString != null) {
			LocalDateTime localDateTime = LocalDateTime.parse(localDateTimeString, formatter);
			return localDateTime.toInstant(ZoneOffset.UTC);
		} else {
			return null;
		}
	}

}
