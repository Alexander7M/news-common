package com.epam.news.common.util;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public class OffsetDateTimeToStringBiConverter implements BiConverter<OffsetDateTime, String> {

	private static final String LOCAL_DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern(LOCAL_DATE_TIME_PATTERN);
	
	@Override
	public String convertForward(OffsetDateTime offsetDateTime) {
		return offsetDateTime.withOffsetSameInstant(ZoneOffset.UTC).format(FORMATTER);
	}

	@Override
	public OffsetDateTime convertBackward(String offsetDateTimeString) {
		LocalDateTime localDateTime = LocalDateTime.parse(offsetDateTimeString, FORMATTER);
		return OffsetDateTime.of(localDateTime, ZoneOffset.UTC);
	}

}
