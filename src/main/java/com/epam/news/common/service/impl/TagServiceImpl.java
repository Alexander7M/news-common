package com.epam.news.common.service.impl;

import static com.epam.news.common.service.util.ListBeanOperations.*;

import java.util.List;

import com.epam.news.common.dao.TagDao;
import com.epam.news.common.dao.exception.DataAccessException;
import com.epam.news.common.entity.Tag;
import com.epam.news.common.service.ServiceException;
import com.epam.news.common.service.TagService;

public class TagServiceImpl implements TagService {

	private TagDao tagDao;
	
	public void setTagDao(TagDao tagDao) {
		this.tagDao = tagDao;
	}
	
	@Override
	public void createTag(Tag tag) throws ServiceException {
		try {
			Long tagId = tagDao.create(tag);
			tag.setId(tagId);
		} catch (DataAccessException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void createTags(List<Tag> tags) throws ServiceException {
		try {
			List<Long> idList = tagDao.create(tags);
			setIds(idList, tags);
		} catch (DataAccessException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Tag getTagByName(String tagName) throws ServiceException {
		try {
			return tagDao.selectByName(tagName);
		} catch (DataAccessException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public List<Tag> getTagsByNames(List<String> name) throws ServiceException {
		try {
			return tagDao.selectByNames(name);
		} catch (DataAccessException e) {
			throw new ServiceException(e);
		}
	}

}
