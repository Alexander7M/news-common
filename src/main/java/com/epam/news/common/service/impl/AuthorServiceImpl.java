package com.epam.news.common.service.impl;

import java.time.Instant;

import com.epam.news.common.dao.AuthorDao;
import com.epam.news.common.dao.exception.DataAccessException;
import com.epam.news.common.entity.Author;
import com.epam.news.common.service.AuthorService;
import com.epam.news.common.service.ServiceException;


public class AuthorServiceImpl implements AuthorService {

	private AuthorDao authorDao;
	
	public void setAuthorDao(AuthorDao authorDao) {
		this.authorDao = authorDao;
	}
	
	@Override
	public void saveAuthor(Author author) throws ServiceException {
		try {
			Long authorId = authorDao.create(author);
			author.setId(authorId);
		} catch (DataAccessException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void deleteAuthor(Author author) throws ServiceException {
		Instant previousEpirationInstanet = author.getExpirationDate();
		try {
			author.setExpirationDate(Instant.now());
			authorDao.updateExpiredField(author);
		} catch (DataAccessException e) {
			author.setExpirationDate(previousEpirationInstanet);
			throw new ServiceException(e);
		}
	}

	@Override
	public Author getAuthorById(Long id) throws ServiceException {
		try {
			return authorDao.selectByKey(id);
		} catch (DataAccessException e) {
			throw new ServiceException(e);
		}
	}
	
}
