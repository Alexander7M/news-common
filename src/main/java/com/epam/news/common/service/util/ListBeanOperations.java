package com.epam.news.common.service.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Function;

import org.apache.log4j.Logger;

import com.epam.news.common.entity.Identifiable;
import com.epam.news.common.entity.Tag;

public class ListBeanOperations {
	
	private static Logger logger = Logger.getLogger(ListBeanOperations.class);
	
	public static <S, T> List<T> map(List<S> sourceList, Function<S, T> mapper) {
		List<T> targetList = new ArrayList<T>(sourceList.size());
		for (S sourceItem : sourceList) {
			T targetItem = mapper.apply(sourceItem);
			targetList.add(targetItem);
		}
		return targetList;
	}
	
	public static <S1, S2> void merge(List<S1> sourceList1, List<S2> sourceList2, BiConsumer<S1, S2> consumer) {
		if (sourceList1.size() != sourceList2.size()) {
			logger.warn("Lists' sizes are not equals in method <ListBeanOperations.merge>");
		}
		Iterator<S1> source1Iterator = sourceList1.iterator();
		Iterator<S2> source2Iterator = sourceList2.iterator();
		while(source1Iterator.hasNext() && source2Iterator.hasNext()) {
			S1 source1 = source1Iterator.next();
			S2 source2 = source2Iterator.next();
			consumer.accept(source1, source2);
		}
	}
	
	public static <IdType, EntityType extends Identifiable<IdType>> List<IdType> getIds(List<EntityType> entityList) {
		return map(entityList, Identifiable::getId);
	}
	
	public static <IdType, EntityType extends Identifiable<IdType>> void  setIds(List<IdType> idList, List<EntityType> entityList) {
		merge(entityList, idList, (entity, id) -> entity.setId(id));
	}
	
	public static List<String> getNamesFromTags(List<Tag> tagList) {
		return map(tagList, t -> t.getName());
	}
		
}
