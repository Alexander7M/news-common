package com.epam.news.common.service.impl;

import static com.epam.news.common.service.util.ListBeanOperations.*;

import java.util.List;
import java.util.stream.Collectors;

import com.epam.news.common.dao.CommentDao;
import com.epam.news.common.dao.exception.DataAccessException;
import com.epam.news.common.entity.Comment;
import com.epam.news.common.service.CommentService;
import com.epam.news.common.service.ServiceException;

public class CommentServiceImpl implements CommentService {

	private CommentDao commentDao;

	public void setCommentDao(CommentDao commentDao) {
		this.commentDao = commentDao;
	}
	
	@Override
	public void saveComment(Comment comment) throws ServiceException {
		try {
			Long commentId = commentDao.create(comment);
			comment.setId(commentId);
		} catch (DataAccessException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public void updateComment(Comment comment) throws ServiceException {
		try {
			commentDao.update(comment);
		} catch (DataAccessException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void saveComments(List<Comment> commentList) throws ServiceException {
		try {
			List<Long> idList = commentDao.create(commentList);
			setIds(idList, commentList);
		} catch (DataAccessException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void deleteComment(Comment comment) throws ServiceException {
		try {
			commentDao.delete(comment.getId());
		} catch (DataAccessException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void deleteComments(List<Comment> comments) throws ServiceException {
		try {
			List<Long> idList = comments.stream().map(comment -> comment.getId()).collect(Collectors.toList());
			commentDao.delete(idList);
		} catch (DataAccessException e) {
			throw new ServiceException(e);
		}
	}
	
}
