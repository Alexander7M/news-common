package com.epam.news.common.service.impl;

import static com.epam.news.common.service.util.ListBeanOperations.getIds;
import static com.epam.news.common.service.util.ServiceUtils.check;
import static com.epam.news.common.service.util.ServiceUtils.identified;
import static com.epam.news.common.service.util.ServiceUtils.notNull;

import java.util.List;
import java.util.stream.Collectors;

import com.epam.news.common.dao.NewsDao;
import com.epam.news.common.dao.exception.DataAccessException;
import com.epam.news.common.dto.NewsFilter;
import com.epam.news.common.dto.NewsSorting;
import com.epam.news.common.dto.Pagination;
import com.epam.news.common.entity.Author;
import com.epam.news.common.entity.News;
import com.epam.news.common.entity.Tag;
import com.epam.news.common.service.NewsService;
import com.epam.news.common.service.ServiceException;

public class NewsServiceImpl implements NewsService {
	
	private NewsDao newsDao;
	
	public void setNewsDao(NewsDao newsDao) {
		this.newsDao = newsDao;
	}
	
	@Override
	public News getNewsById(Long id) throws ServiceException {
		try {
			return newsDao.selectByKey(id);
		} catch (DataAccessException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public void saveNews(News news) throws ServiceException {
		try {
			Long newsId = newsDao.create(news);
			news.setId(newsId);
		} catch (DataAccessException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void updateNews(News news) throws ServiceException {
		try {
			check(notNull(news) && identified(news));
			newsDao.update(news);
		} catch (DataAccessException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void deleteNews(News news)throws ServiceException {
		try {
			check(notNull(news) && identified(news));
			newsDao.delete(news.getId());
		} catch (DataAccessException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public void addNewsAuthor(News news, Author author) throws ServiceException {
		try {
			check(notNull(news), notNull(author), identified(news) && identified(author));
			newsDao.addNewsAuthor(news.getId(), author.getId());
		} catch (DataAccessException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public void addNewsTag(News news, Tag tag) throws ServiceException {
		try {
			check(notNull(news), notNull(tag), identified(news) && identified(tag));
			newsDao.addNewsTag(news.getId(), tag.getId());
		} catch (DataAccessException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void addNewsTags(News news, List<Tag> tags) throws ServiceException {
		try {
			List<Long> tagIds = tags.stream().map(tag -> tag.getId()).collect(Collectors.toList());
			newsDao.addNewsTags(news.getId(), tagIds);
		} catch (DataAccessException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public List<News> getAllNews() throws ServiceException {
		try {
			return newsDao.selectAllWithCommentCountSorting();
		} catch (DataAccessException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<News> getNewsListWritedByAuthor(Author author) throws ServiceException {
		try {
			check(notNull(author) && identified(author));
			return newsDao.selectByAuthor(author.getId());
		} catch (DataAccessException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<News> getNewsListMarkedByTags(List<Tag> tagList) throws ServiceException {
		try {
			List<Long> tagsIds = getIds(tagList);
			return newsDao.selectByTags(tagsIds);
		} catch (DataAccessException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<News> getNewsList(NewsFilter newsFilter, NewsSorting soringType, Pagination pagination) throws ServiceException {
		try {
			return newsDao.selectHeaders(newsFilter, soringType, pagination);
		} catch (DataAccessException e) {
			throw new ServiceException(e);
		}
	}

}
