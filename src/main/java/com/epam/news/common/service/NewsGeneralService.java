package com.epam.news.common.service;

import java.util.List;

import com.epam.news.common.entity.Author;
import com.epam.news.common.entity.News;

/**
 * A compound service for news. 
 */
public interface NewsGeneralService {

	/** 
	 * Marks news with geiven tag in database.<br>
	 * Equals to seqence of actions: get tag form database, set this tag for news.
	 * @param news is news to be marked by given tag. Must be not null and identified.
	 * @param tagName is name of tag to be set for news. Tag with this name must exist in database.
	 * @throws ServiceException when some databse exception occures, parameters are wrong or tag with given name doesn't exist.
	 * @see TagService#getTagByName(String)
	 * @see NewsService#addNewsTag(News, Tag)
	 */
	void addNewsTag(News news, String tagName) throws ServiceException;
	
	/**
	 * Marks news with geiven tags.<br>
	 * Equals to sequence of actions: get tags from database, set this tags for news.
	 * @param news is news to be marked. Must be not null and identified.
	 * @param tagNameList is list of tags' names  to be set for news. Each of this tegs must exist in databse.
	 * @throws ServiceException when some databse exception occures, parameters are wrong or some of tags doesn't exist in database.
	 * @see TagService#getTagsByNames(List)
	 * @see NewsService#addNewsTags(News, List)
	 */
	void addNewsTags(News news, List<String> tagNameList) throws ServiceException;
	
	/**
	 * Saves news in database, sets her author and tags.<br>
	 * Equals to sequence of actions: seve news, add news author, add news tags.
	 * @param news is news to be saved.
	 * @param author is author of news. Author must be not null and identified.
	 * @param tagNameList is list of news tag names. Each of tag should exist in database.
	 * @throws ServiceException when some databse exception occures or parameters are wrong.
	 * @see NewsService#saveNews(News)
	 * @see NewsService#addNewsAuthor(News, Author)
	 * @see NewsService#addNewsTags(News, List)
	 */
	void saveNewsWithAuthorAndTags(News news, Author author, List<String> tagNameList) throws ServiceException;
	
}
