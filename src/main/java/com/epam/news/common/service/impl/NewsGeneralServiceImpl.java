package com.epam.news.common.service.impl;

import static com.epam.news.common.service.util.ServiceUtils.check;
import static com.epam.news.common.service.util.ServiceUtils.identified;
import static com.epam.news.common.service.util.ServiceUtils.notNull;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.epam.news.common.entity.Author;
import com.epam.news.common.entity.News;
import com.epam.news.common.entity.Tag;
import com.epam.news.common.service.NewsGeneralService;
import com.epam.news.common.service.NewsService;
import com.epam.news.common.service.ServiceException;
import com.epam.news.common.service.TagService;

public class NewsGeneralServiceImpl implements NewsGeneralService {

	private NewsService newsService;
	private TagService tagService;
	
	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}
	
	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}
	
	@Override
	@Transactional(rollbackFor = ServiceException.class)
	public void saveNewsWithAuthorAndTags(News news, Author author, List<String> tags) throws ServiceException {
		try {
			saveNews(news);
			addNewsAuthor(news, author);
			addNewsTags(news, tags);
		} catch (ServiceException e) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			throw e;
		}
	}

	@Override
	public void addNewsTag(News news, String tagName) throws ServiceException {
		Tag tag = tagService.getTagByName(tagName);
		check(notNull(tag));
		newsService.addNewsTag(news, tag);
	}
	
	@Override
	public void addNewsTags(News news, List<String> tagNameList) throws ServiceException {
		List<Tag> tagList = tagService.getTagsByNames(tagNameList);
		check(tagList.size() == tagNameList.size());
		newsService.addNewsTags(news, tagList);
	}

	public void saveNews(News news) throws ServiceException {
		check(notNull(news));
		newsService.saveNews(news);
	}
	
	public void addNewsAuthor(News news, Author author) throws ServiceException {
		check(notNull(news), identified(news), notNull(author) && identified(author));
		newsService.addNewsAuthor(news, author);
	}
	
}
