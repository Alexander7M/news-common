package com.epam.news.common.service.util;

import com.epam.news.common.entity.Identifiable;
import com.epam.news.common.service.ServiceException;

public class ServiceUtils {

	public static void check(boolean ... conditions) throws ServiceException {
		boolean accumulation = true;
		for (int i=0; i<conditions.length & accumulation; i++) {
			accumulation &= conditions[i];
		} 
		check(accumulation);
	}
	
	public static void check(boolean condition) throws ServiceException {
		if (!condition) {
			throw new ServiceException("method condition isn't complied with");
		}
	}
	
	public static boolean notNull(Identifiable<?> entity) {
		return entity != null;
	}
	
	public static boolean identified(Identifiable<?> entity) {
		return entity.isIdentified();
	}
	
}
