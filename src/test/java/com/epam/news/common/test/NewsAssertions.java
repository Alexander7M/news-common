package com.epam.news.common.test;

import static java.lang.Math.abs;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.junit.Assert;

import com.epam.news.common.entity.Author;
import com.epam.news.common.entity.Comment;
import com.epam.news.common.entity.News;
import com.epam.news.common.entity.Tag;

public class NewsAssertions extends Assert {

	public static void assertEqualsAuthors(Author expected, Author actual) {
		assertEquals(expected.getName(), actual.getName());
		assertEquals(expected.getExpirationDate(), actual.getExpirationDate());
	}
	
	public static void assertEqualsNews(News expected, News actual) {
		assertEquals(expected.getTitle(), actual.getTitle());
		assertEquals(expected.getShortText(), actual.getShortText());
		assertEquals(expected.getFullText(), actual.getFullText());
		assertEquals(expected.getCreationDate(), actual.getCreationDate());
		assertEqualsDates(expected.getModificationDate(), actual.getModificationDate());
	}
	
	public static void assertEqualsDates(Date expected, Date actual) {
		assertTrue( abs(expected.getTime() - actual.getTime()) < 1000 );
	}
	
	public static void assertEqualsTags(Tag expected, Tag actual) {
		assertEquals(expected.getName(), actual.getName());
	}
	
	public static <T> void assertEquals(List<T> expected, List<T> actual) {
		assertEquals(expected.size(), actual.size());
		Iterator<T> itOfExpected = expected.iterator();
		Iterator<T> itOfActual = actual.iterator();
		while (itOfExpected.hasNext() && itOfActual.hasNext()) {
			assertEquals(itOfExpected.next(), itOfActual.next());
		}
	}
	
	public static void assertEqualsHeaders(News expected, News actual) {
		assertEquals(expected.getTitle(), actual.getTitle());
		assertEquals(expected.getShortText(), actual.getShortText());
		assertEquals(expected.getCreationDate(), actual.getCreationDate());
		assertEqualsDates(expected.getModificationDate(), actual.getModificationDate());
	}
	
	public static void assertEqualsComments(Comment expected, Comment actual) {
		assertEquals(expected.getText(), actual.getText());
		assertEquals(expected.getCreationDate(), actual.getCreationDate());
	}
	
	public static void assertEqualsLinks(Object expected, Object actual) {
		assertTrue("objects are not equals", expected == actual);
	}
	
}
