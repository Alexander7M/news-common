package com.epam.news.common.test.fakefactory;

import static com.epam.news.common.service.util.ListBeanOperations.setIds;
import static com.epam.news.common.test.fakefactory.IdFakeFactory.getFakeLongId;
import static com.epam.news.common.test.fakefactory.IdFakeFactory.getFakeLongIdList;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.epam.news.common.entity.News;

public class NewsFakeFactory {
		
	public static News getFakeNews() {
		News fakeNews = new News();
		fakeNews.setTitle("Fake Title");
		fakeNews.setShortText("Fake Short Text");
		fakeNews.setFullText("Fake Full Text");
		fakeNews.setCreationDate(Instant.now());
		fakeNews.setModificationDate(Date.from(Instant.now()));
		return fakeNews;
	}
	
	public static News getFakeNews(int uniqueIndex) {
		News fakeNews = getFakeNews();
		fakeNews.setTitle(fakeNews.getTitle() + " #" + uniqueIndex);
		fakeNews.setShortText(fakeNews.getShortText() + " #" + uniqueIndex);
		fakeNews.setFullText(fakeNews.getFullText() + " #" + uniqueIndex);
		return fakeNews;
	}
	
	public static News getFakeNewsWithId(Long id) {
		News fakeNews = getFakeNews();
		fakeNews.setId(id);
		return fakeNews;
	}
	
	public static News getFakeNewsWithId() {
		return getFakeNewsWithId(getFakeLongId());
	}
	
	public static List<News> getFakeNewsList(int size) {
		List<News> fakeNewsList = new ArrayList<News>(size);
		for (int i=0; i<size; i++) {
			News fakeNews = getFakeNews(i + 42);
			fakeNewsList.add(fakeNews);
		}
		return fakeNewsList;
	}
	
	public static List<News> getFakeNewsListWithIds(int size) {
		List<News> fakeNewsList = getFakeNewsList(size);
		List<Long> fakeIdList = getFakeLongIdList(size);
		setIds(fakeIdList, fakeNewsList);
		return fakeNewsList;
	}
	
}