package com.epam.news.common.test.fakefactory;

import static com.epam.news.common.service.util.ListBeanOperations.*;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import com.epam.news.common.entity.Comment;

public class CommentFakeFactory {

	public static Comment getFakeComment() {
		Comment fakeComment = new Comment();
		fakeComment.setText("Fake Comment");
		fakeComment.setCreationDate(Instant.now());
		return fakeComment;
	}
	
	public static Comment getIrregualFakeComment() {
		StringBuilder largeTextBuilder = new StringBuilder();
		for (int i=0; i<100; i++) {
			largeTextBuilder.append("<Large Text>");
		}
		String largeText = largeTextBuilder.toString();
		Comment fakeComment = new Comment();
		fakeComment.setText(largeText);
		fakeComment.setCreationDate(Instant.now());
		return fakeComment;
	}
	
	public static Comment getFakeComment(int uniqueIndex) {
		Comment fakeComment = getFakeComment();
		fakeComment.setText( fakeComment.getText() + " #" + uniqueIndex );
		return fakeComment;
	}
	
	public static Comment getFakeCommentWithId() {
		Comment fakeComment = getFakeComment();
		Long fakeId = IdFakeFactory.getFakeLongId();
		fakeComment.setId(fakeId);
		return fakeComment;
	}
	
	public static List<Comment> getFakeCommentList(int size) {
		List<Comment> list = new ArrayList<Comment>(size);
		for (int i=0; i<size; i++) {
			Comment fakeComment = getFakeComment(i);
			list.add(fakeComment);
		}
		return list;
	}

	public static List<Comment> getFakeCommentListWithIds(int size) {
		List<Long> fakeIdList = IdFakeFactory.getFakeLongIdList(size);
		return getFakeCommentListWithIds(size, fakeIdList);
	}
	
	public static List<Comment> getFakeCommentListWithIds(int size, List<Long> idList) {
		List<Comment> fakeCommentList = getFakeCommentList(size);
		setIds(idList, fakeCommentList);
		return fakeCommentList;
	}
	
}