package com.epam.news.common.test.fakefactory;

import java.util.ArrayList;
import java.util.List;

import com.epam.news.common.entity.Author;

public class AuthorFakeFactory {
	
	public static Author getFakeAuthor() {
		return new Author("���������� �����");
	}
	
	public static Author getFakeAuthorWithId() {
		return getFakeAuthorWithId(IdFakeFactory.getFakeLongId());
	}
	
	public static Author getFakeAuthorWithId(Long id) {
		Author fakeAuthor = getFakeAuthor();
		fakeAuthor.setId(id);
		return fakeAuthor;
	}
	
	public static List<Author> getFakeAuthorList(int size) {
		List<Author> list = new ArrayList<Author>(size);
		for(int i=0; i<size; i++) {
			list.add(new Author("Alxander #" + (i+1)));
		}
		return list;
	}
	
}