package com.epam.news.common.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class NewsAppTestContext {

	private NewsAppTestContext() {
		
	}
	
	private static final class ContextHolder {
		private static final ApplicationContext context = new ClassPathXmlApplicationContext("classpath:com/epam/news/common/test/context.xml");
	}
	
	public static Object getBean(String name) {
		return ContextHolder.context.getBean(name);
	}

	public static <T> T getBean(String name, Class<T> requiredType){
		return ContextHolder.context.getBean(name, requiredType);
	}
	
	public static <T> T getBean(Class<T> requiredType){
		return ContextHolder.context.getBean(requiredType);
	}
	
}
