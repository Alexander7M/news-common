package com.epam.news.common.test.fakefactory;

import java.util.ArrayList;
import java.util.List;

public class IdFakeFactory {
	
	public static Long getFakeLongId() {
		return new Long(42L);
	}
	
	public static List<Long> getFakeLongIdList(int size) {
		List<Long> idList = new ArrayList<Long>(size);
		for(long i = 0; i < size; i++) {
			idList.add(new Long(42 + i));
		}
		return idList;
	}
	
}