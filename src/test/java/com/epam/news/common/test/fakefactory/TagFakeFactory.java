package com.epam.news.common.test.fakefactory;

import static com.epam.news.common.service.util.ListBeanOperations.*;
import static com.epam.news.common.test.fakefactory.IdFakeFactory.*;

import java.util.ArrayList;
import java.util.List;

import com.epam.news.common.entity.Tag;

public class TagFakeFactory {

	public static String getFakeTagName() {
		return "FakeTag";
	}
	
	public static String getFakeTagName(int index) {
		return getFakeTagName() + " #" + index;
	}
	
	public static List<String> getFakeTagNameList(int size) {
		List<String> fakeTagNameList = new ArrayList<String>(size);
		for (int i=0; i<size; i++) {
			String fakeTagName = getFakeTagName( 42 + i );
			fakeTagNameList.add(fakeTagName);
		}
		return fakeTagNameList;
	}
	
	public static Tag getFakeTag() {
		return new Tag("FakeTag");
	}
	
	public static Tag getFakeLargeTag() {
		StringBuilder largeTagBuilder = new StringBuilder();
		for (int i=0; i<100; i++) {
			largeTagBuilder.append("abc");
		}
		String largeTagName = largeTagBuilder.toString();
		return new Tag(largeTagName);
	}
	
	public static Tag getFakeTag(int uniqueIndex) {
		Tag fakeTag = getFakeTag();
		fakeTag.setName(fakeTag.getName() + " #" + uniqueIndex);
		return fakeTag;
	}
	
	public static Tag getFakeTagWithId() {
		Tag fakeTag = getFakeTag();
		Long fakeId = getFakeLongId();
		fakeTag.setId(fakeId);
		return fakeTag;
	}
	
	public static List<Tag> getFakeTagList(int size) {
		List<Tag> list = new ArrayList<Tag>(size);
		for(int i=0; i<size; i++) {
			Tag fakeTag = getFakeTag(i + 42);
			list.add(fakeTag);
		}
		return list;
	}
	
	public static List<Tag> getFakeTagListWithId(int size) {
		List<Tag> fakeTagList = getFakeTagList(size);
		List<Long> fakeIdList = getFakeLongIdList(size);
		setIds(fakeIdList, fakeTagList);
		return fakeTagList;
	}
	
}