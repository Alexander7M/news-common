package com.epam.news.common.dao.test;

import java.util.Locale;

import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@DbUnitConfiguration(databaseConnection = "dbUnitDatabaseConnection")
@ContextConfiguration(locations = { "classpath:com/epam/news/common/test/context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		TransactionDbUnitTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public abstract class AppDaoTest {

	static {
		Locale.setDefault(Locale.ENGLISH);
		new DOMConfigurator().doConfigure("log4j.xml", LogManager.getLoggerRepository());
	}
	
}