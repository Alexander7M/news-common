package com.epam.news.common.dao.test;

import static com.epam.news.common.test.fakefactory.TagFakeFactory.getFakeTag;
import static com.epam.news.common.test.fakefactory.TagFakeFactory.getFakeTagList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;

import com.epam.news.common.dao.TagDao;
import com.epam.news.common.entity.Tag;
import com.epam.news.common.test.NewsAppTestContext;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;

@DatabaseSetup({"classpath:dataset/input/tag.xml", "classpath:dataset/input/void.xml"})
public class TagDaoTest extends AppDaoTest {

	private TagDao tagDao = NewsAppTestContext.getBean("tagDao", TagDao.class);
	
	@Test
	public void textSelectAll() throws Exception {
		List<Tag> allTags = tagDao.selectAllWithCommentCountSorting();
		assertEquals(11, allTags.size());
	}
	
	@Test
	public void testCreate() throws Exception {
		Tag fakeTag = getFakeTag();
		Long fakeId = tagDao.create(fakeTag);
		assertNotNull(fakeId);
		Tag createdTag = tagDao.selectByKey(fakeId);
		assertEquals(fakeTag, createdTag);
	}
	
	
	@Test
	public void testUpdate() throws Exception {
		Tag fakeTag = getFakeTag();
		Long fakeId = tagDao.create(fakeTag);
		fakeTag.setId(fakeId);
		fakeTag.setName("updated_name");
		tagDao.update(fakeTag);
		Tag selectedTag = tagDao.selectByKey(fakeId);
		assertEquals(fakeTag, selectedTag);
	}
	
	@Test
	public void testDelete() throws Exception {
		Tag fakeTag = getFakeTag();
		Long fakeId = tagDao.create(fakeTag);
		tagDao.delete(fakeId);
		Tag selectedTag = tagDao.selectByKey(fakeId);
		assertNull(selectedTag);
	}
	
	@Test
	public void testSelectByKey() throws Exception {
		Tag fakeTag = getFakeTag();
		Long fakeId = tagDao.create(fakeTag);
		Tag selectedTag = tagDao.selectByKey(fakeId);
		assertEquals(fakeTag, selectedTag);
	}
	
	@Test
	public void testSelectByName() throws Exception {
		Tag fakeTag = getFakeTag();
		Long fakeId = tagDao.create(fakeTag);
		Tag selectedTag = tagDao.selectByName(fakeTag.getName());
		assertEquals(fakeId, selectedTag.getId());
		assertEquals(fakeTag, selectedTag);
	}
	
	
	@Test
	public void testSelectByKeys() throws Exception {
		int listSize = 5;
		List<Long> idList = new ArrayList<Long>(listSize);
		for (int i=1; i<=listSize; i++) {
			idList.add(new Long(i));
		}
		List<Tag> selectedTags = tagDao.selectByKeys(idList);
		assertEquals(idList.size(), selectedTags.size());
	}
	
	@Test
	public void testSelectByNames() throws Exception {
		List<Tag> fakeTagList = getFakeTagList(10);
		tagDao.create(fakeTagList);
		List<String> fakeNameList = fakeTagList.stream().map(tag -> tag.getName()).collect(Collectors.toList());
		List<Tag> selectedTagList = tagDao.selectByNames(fakeNameList);
		assertEquals(fakeTagList.size(), selectedTagList.size());
	}
	
	@Test
	public void testBatchCreate() throws Exception {
		List<Tag> fakeTagList = getFakeTagList(10);
		List<Long> fakeIdList = tagDao.create(fakeTagList);
		List<Tag> selectedTagList = tagDao.selectByKeys(fakeIdList);
		assertEquals(fakeTagList.size(), selectedTagList.size());
	}
	
	@Test
	public void testBatchUpdate() throws Exception {
		List<Tag> fakeTagList = getFakeTagList(10);
		List<Long> fakeIdList = tagDao.create(fakeTagList);
		Iterator<Tag> itOfTagList = fakeTagList.iterator();
		Iterator<Long> itOfIdList = fakeIdList.iterator();
		while(itOfTagList.hasNext()) {
			itOfTagList.next().setId(itOfIdList.next());
		}
		fakeTagList.forEach((tag) -> tag.setName(tag.getName() + "v2"));
		tagDao.update(fakeTagList);
		List<Tag> selectedTagList = tagDao.selectByKeys(fakeIdList);
		assertEquals(fakeTagList.size(), selectedTagList.size());
		fakeTagList.sort((tag1, tag2) -> tag1.getName().compareTo(tag2.getName()));
		selectedTagList.sort((tag1, tag2) -> tag1.getName().compareTo(tag2.getName()));
	}
	
	@Test
	@ExpectedDatabase(value="classpath:dataset/output/void.xml", table="tag")
	public void testBatchDelete() throws Exception {
		List<Tag> allTags = tagDao.selectAllWithCommentCountSorting();
		List<Long> allIds = new ArrayList<Long>(allTags.size());
		for(Tag tag : allTags) {
			allIds.add(tag.getId());
		}
		tagDao.delete(allIds);
	}
	
}
