package com.epam.news.common.dao.test;

import static com.epam.news.common.service.util.ListBeanOperations.getIds;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import com.epam.news.common.dao.NewsDao;
import com.epam.news.common.dto.NewsFilter;
import com.epam.news.common.dto.NewsSorting;
import com.epam.news.common.dto.Pagination;
import com.epam.news.common.test.NewsAppTestContext;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@DatabaseSetup({"classpath:dataset/input/news.xml", 
	"classpath:dataset/input/author.xml", 
	"classpath:dataset/input/news_author.xml",
	"classpath:dataset/input/tag.xml",
	"classpath:dataset/input/news_tag.xml",
	"classpath:dataset/input/comments.xml",
	"classpath:dataset/input/void.xml"})
public class NewsDaoSelectionTest extends AppDaoTest {
	
	private final NewsDao newsDao = NewsAppTestContext.getBean("newsDao", NewsDao.class);
	
	public static class TestCase {
		public final NewsFilter filter;
		public final NewsSorting sorting;
		public final Pagination pagination;
		public final List<Long> expected;
		public TestCase(NewsFilter filter, NewsSorting sorting, Pagination pagination, List<Long> expected) {
			this.filter = filter;
			this.sorting = sorting;
			this.pagination = pagination;
			this.expected = expected;
		}
	}
	
	private static final List<TestCase> testCaseSuite = asList(
			new TestCase(new NewsFilter(1L, asList(2L, 3L)), new NewsSorting(), new Pagination(1), asList(2L, 1L)),
			new TestCase(new NewsFilter(1L, asList(3L, 2L)), new NewsSorting(), new Pagination(1), asList(2L, 1L)),
			new TestCase(new NewsFilter(null, asList(2L, 3L)), new NewsSorting(), new Pagination(1), asList(2L, 3L, 1L)),
			new TestCase(new NewsFilter(1L, null), new NewsSorting(), new Pagination(1), asList(5L, 2L, 1L)),
			new TestCase(new NewsFilter(1L, asList()), new NewsSorting(), new Pagination(1), asList(5L, 2L, 1L)),
			new TestCase(new NewsFilter(null, null), new NewsSorting(), new Pagination(1), asList(5L, 4L, 2L, 3L, 1L))
	);
	
	@Test
	public void test() throws Exception {
		for(int test=0; test<testCaseSuite.size(); test++) {
			TestCase testCase = testCaseSuite.get(test);
			List<Long> actual = getIds(newsDao.selectHeaders(testCase.filter, testCase.sorting, testCase.pagination));
			assertEquals(String.format("Test #%d failed", test+1), testCase.expected, actual);
		}
	}
	
}
