package com.epam.news.common.dao.test;

import static com.epam.news.common.test.NewsAssertions.assertEqualsHeaders;
import static com.epam.news.common.test.NewsAssertions.assertEqualsNews;
import static com.epam.news.common.test.fakefactory.AuthorFakeFactory.getFakeAuthor;
import static com.epam.news.common.test.fakefactory.NewsFakeFactory.getFakeNews;
import static com.epam.news.common.test.fakefactory.TagFakeFactory.getFakeTag;
import static org.hamcrest.collection.IsIn.isIn;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import com.epam.news.common.dao.AuthorDao;
import com.epam.news.common.dao.NewsDao;
import com.epam.news.common.dao.TagDao;
import com.epam.news.common.entity.Author;
import com.epam.news.common.entity.News;
import com.epam.news.common.entity.Tag;
import com.epam.news.common.test.NewsAppTestContext;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@DatabaseSetup({"classpath:dataset/input/news.xml", 
	"classpath:dataset/input/author.xml", 
	"classpath:dataset/input/news_author.xml",
	"classpath:dataset/input/tag.xml",
	"classpath:dataset/input/news_tag.xml",
	"classpath:dataset/input/void.xml"})
public class NewsDaoTest extends AppDaoTest {
	
	private final NewsDao newsDao = NewsAppTestContext.getBean("newsDao", NewsDao.class);
	private final AuthorDao authorDao = NewsAppTestContext.getBean("authorDao", AuthorDao.class);
	private final TagDao tagDao = NewsAppTestContext.getBean("tagDao", TagDao.class);
	
	@Test
	public void testCreate() throws Exception {
		News fakeNews = getFakeNews();
		Long fakeId = newsDao.create(fakeNews);
		News createdNews = newsDao.selectByKey(fakeId);
		assertEqualsNews(fakeNews, createdNews);
	}
	
	@Test
	public void testSelectAll() throws Exception {
		List<News> allNews = newsDao.selectAllWithCommentCountSorting();
		assertEquals(5, allNews.size());
	}
		
	@Test
	public void testSelectByKey() throws Exception {
		News fakeNews = getFakeNews();
		Long fakeId = newsDao.create(fakeNews);
		News selectedNews = newsDao.selectByKey(fakeId);
		assertEqualsNews(fakeNews, selectedNews);
	}
	
	@Test()
	public void testDelete() throws Exception {
		News fakeNews = getFakeNews();
		Long fakeId = newsDao.create(fakeNews);
		newsDao.delete(fakeId);
		assertNull(newsDao.selectByKey(fakeId));
	}
	
	@Test
	public void testUpdate() throws Exception {
		News fakeNews = getFakeNews();
		Long fakeId = newsDao.create(fakeNews);
		fakeNews.setId(fakeId);
		fakeNews.setTitle("Updated Fake Title");
		fakeNews.setShortText("Updated Short Title");
		fakeNews.setFullText("Updated Full Text");
		fakeNews.setCreationDate(Instant.now().minusMillis(1000));
		fakeNews.setModificationDate(Date.from(Instant.now()));
		newsDao.update(fakeNews);
		News selectedNews = newsDao.selectByKey(fakeId);
		assertEqualsNews(fakeNews, selectedNews);
	}

	@Test
	public void testSelectByAuthor() throws Exception {
		List<News> newsList = newsDao.selectByAuthor(new Long(1));
		assertEquals(3, newsList.size());
	}
	
	@Test
	public void testAddNewsAuthor() throws Exception {
		Author fakeAuthor = getFakeAuthor();
		News fakeNews = getFakeNews();
		Long fakeNewsId = newsDao.create(fakeNews);
		Long fakeAuthorId = authorDao.create(fakeAuthor);
		newsDao.addNewsAuthor(fakeNewsId, fakeAuthorId);
		List<News> selectedNewsList = newsDao.selectByAuthor(fakeAuthorId);
		assertEquals(1, selectedNewsList.size());
		News selectedNews = selectedNewsList.get(0);
		assertEqualsHeaders(fakeNews, selectedNews);
	}

	@Test
	public void testDeleteNewsAuthor() throws Exception {
		Author fakeAuthor = getFakeAuthor();
		News fakeNews = getFakeNews();
		Long fakeNewsId = newsDao.create(fakeNews);
		Long fakeAuthorId = authorDao.create(fakeAuthor);
		newsDao.addNewsAuthor(fakeNewsId, fakeAuthorId);
		newsDao.deleteNewsAuthor(fakeNewsId, fakeAuthorId);
		List<News> selectedNewsList = newsDao.selectByAuthor(fakeAuthorId);
		assertEquals(0, selectedNewsList.size());
	}
	
	@Test
	public void testSelectByTags() throws Exception {
		List<News> selectedNews = newsDao.selectByTags(Arrays.asList(new Long(1)));
		assertEquals(2, selectedNews.size());
		assertThat(new News(new Long(1)), isIn(selectedNews));
		assertThat(new News(new Long(1)), isIn(selectedNews));
	}
	
	@Test
	public void testAddNewsTags() throws Exception {
		News fakeNews = getFakeNews();
		Tag fakeTag = getFakeTag();
		Long fakeNewsId = newsDao.create(fakeNews);
		Long fakeTagId = tagDao.create(fakeTag);
		fakeNews.setId(fakeNewsId);
		fakeTag.setId(fakeTagId);
		newsDao.addNewsTag(fakeNewsId, fakeTagId);
		List<News> selectedNews = newsDao.selectByTags(Arrays.asList(fakeTagId));
		assertEquals(1, selectedNews.size());
		assertEqualsHeaders(fakeNews, selectedNews.get(0));
	}
	
	@Test
	public void testAddNewsTag() throws Exception {
		News fakeNews = getFakeNews();
		Tag fakeTag = getFakeTag();
		Long fakeNewsId = newsDao.create(fakeNews);
		Long fakeTagId = tagDao.create(fakeTag);
		fakeNews.setId(fakeNewsId);
		fakeTag.setId(fakeTagId);
		newsDao.addNewsTag(fakeNewsId, fakeTagId);
		List<News> selectedNews = newsDao.selectByTags(Arrays.asList(fakeTagId));
		assertEquals(1, selectedNews.size());
		assertEqualsHeaders(fakeNews, selectedNews.get(0));
	}


	@Test
	public void testDeleteNewsTags() throws Exception {
		News fakeNews = getFakeNews();
		Long tagId = new Long(1);
		Long fakeNewsId = newsDao.create(fakeNews);
		fakeNews.setId(fakeNewsId);
		newsDao.addNewsTag(fakeNewsId, tagId);
		newsDao.deleteNewsTag(fakeNewsId, tagId);
		List<News> selectedNews = newsDao.selectByTags(Arrays.asList(tagId));
		assertThat(fakeNews, not(isIn(selectedNews)));
	}

	@Test
	public void testDeleteNewsTag() throws Exception {
		News fakeNews = getFakeNews();
		Tag fakeTag = getFakeTag();
		Long fakeNewsId = newsDao.create(fakeNews);
		Long fakeTagId = tagDao.create(fakeTag);
		fakeNews.setId(fakeNewsId);
		fakeTag.setId(fakeTagId);
		newsDao.addNewsTag(fakeNewsId, fakeTagId);
	}
	
}
