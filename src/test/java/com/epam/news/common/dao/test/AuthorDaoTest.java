package com.epam.news.common.dao.test;

import static com.epam.news.common.test.NewsAssertions.assertEqualsAuthors;
import static com.epam.news.common.test.fakefactory.AuthorFakeFactory.getFakeAuthor;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.time.Instant;
import java.util.List;

import org.junit.Test;

import com.epam.news.common.dao.AuthorDao;
import com.epam.news.common.dao.exception.DataAccessException;
import com.epam.news.common.entity.Author;
import com.epam.news.common.test.NewsAppTestContext;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@DatabaseSetup({"classpath:dataset/input/author.xml",
		"classpath:dataset/input/void.xml"})
public class AuthorDaoTest extends AppDaoTest {
	
	private AuthorDao authorDao = NewsAppTestContext.getBean("authorDao", AuthorDao.class);
	
	/*
	 * this test assumes, that method delete is correct
	 */
	@Test
	public void testSelectAll() throws Exception {
		List<Author> allAuthors = authorDao.selectAllWithCommentCountSorting();
		assertEquals(5, allAuthors.size());
	}
	
	/*
	 * this test assumes, that method create is correct
	 */
	@Test
	public void testSelectByKey() throws DataAccessException {
		Author insertedAuthor = getFakeAuthor();
		Long insertedId = authorDao.create(insertedAuthor);
		assertNotNull(insertedId);
		Author selectedAuthor = authorDao.selectByKey(insertedId);
		assertEqualsAuthors(insertedAuthor, selectedAuthor);
	}
	
	/*
	 * this test assumes, that method create is correct
	 */
	@Test
	public void testSelectByName() throws Exception {
		Author insertedAuthor = getFakeAuthor();
		int insertedNumber = 23;
		for(int i=0; i<insertedNumber; i++) {
			authorDao.create(insertedAuthor);
		}
		List<Author> selectedAuthors = authorDao.selectByName(insertedAuthor.getName());
		assertEquals(selectedAuthors.size(), insertedNumber);
		for (Author selectedAuthor: selectedAuthors) {
			assertEquals(insertedAuthor.getName(), selectedAuthor.getName());
		}
	}	
	
	/*
	 * this test assumes, that method selectByKey is correct
	 */
	@Test
	public void testCreate() throws Exception {
		Author insertedAuthor = getFakeAuthor();
		Long insertedId = authorDao.create(insertedAuthor);
		assertNotNull(insertedId);
		Author createdAuthor = authorDao.selectByKey(insertedId);
		assertEqualsAuthors(insertedAuthor, createdAuthor);
	}
	
	/*
	 * this test assumes, that method create and method selectByKey is correct
	 */
	@Test()
	public void testDelete() throws Exception {
		Author newAuthor = getFakeAuthor();
		Long authorId = authorDao.create(newAuthor);
		authorDao.delete(authorId);
		Author selectedAuthor = authorDao.selectByKey(authorId);
		assertNull(selectedAuthor);
	}
	
	/*
	 * this test assumes, that method create and method selectByKey is correct
	 */
	@Test
	public void testUpdate() throws DataAccessException {
		Author author = getFakeAuthor();
		Long authorId  = authorDao.create(author);
		author.setId(authorId);
		author.setName("Semenkevich");
		author.setExpirationDate(Instant.now());
		authorDao.update(author);
		Author selectedAuthor = authorDao.selectByKey(authorId);
		assertEqualsAuthors(author, selectedAuthor);
	}
	
	/*
	 * this test assumes, that method create and method selectByKey is correct
	 */
	@Test
	public void testUpdateExpiredField() throws Exception {
		Author author = getFakeAuthor();
		author.setExpirationDate(Instant.now().minusSeconds(60));
		Long authorId  = authorDao.create(author);
		author.setId(authorId);
		author.setExpirationDate(Instant.now());
		authorDao.update(author);
		Author selectedAuthor = authorDao.selectByKey(authorId);
		assertEquals(author.getExpirationDate(), selectedAuthor.getExpirationDate());
	}
	
	/*
	 * this test assumes, that method create and method selectByKey is correct
	 */
	@Test
	public void testUpdateNameField() throws Exception {
		Author author = getFakeAuthor();
		Long authorId  = authorDao.create(author);
		author.setId(authorId);
		author.setName("Semenkevich");
		authorDao.update(author);
		Author selectedAuthor = authorDao.selectByKey(authorId);
		assertEquals(author.getName(), selectedAuthor.getName());
	}
	
}
