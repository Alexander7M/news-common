package com.epam.news.common.dao.test;

import static com.epam.news.common.service.util.ListBeanOperations.*;
import static com.epam.news.common.test.NewsAssertions.assertEqualsComments;
import static com.epam.news.common.test.fakefactory.CommentFakeFactory.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import com.epam.news.common.dao.CommentDao;
import com.epam.news.common.entity.Comment;
import com.epam.news.common.test.NewsAppTestContext;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@DatabaseSetup({
	"classpath:dataset/input/news.xml",
	"classpath:dataset/input/comments.xml",
	"classpath:dataset/input/void.xml"})
public class CommentDaoTest extends AppDaoTest {
	
	private CommentDao commentDao = NewsAppTestContext.getBean("commentDao", CommentDao.class);
	
	@Test
	public void testSelectAll() throws Exception {
		List<Comment> allComments = commentDao.selectAllWithCommentCountSorting();
		assertEquals(15, allComments.size());
	}
	
	@Test
	public void testSelectByKey() throws Exception {
		Long commentId = new Long(1);
		Comment selectedComment = commentDao.selectByKey(commentId);
		assertNotNull(selectedComment);
		assertEquals(commentId, selectedComment.getId());
	}
	
	@Test
	public void testCreate() throws Exception {
		Comment fakeComment = getFakeComment();
		fakeComment.setNewsId(new Long(1));
		Long fakeId = commentDao.create(fakeComment);
		Comment createdComment = commentDao.selectByKey(fakeId);
		assertEqualsComments(fakeComment, createdComment);
	}

	@Test
	public void testUpdate() throws Exception {
		Comment fakeComment = getFakeComment();
		fakeComment.setNewsId(new Long(1));
		Long fakeId = commentDao.create(fakeComment);
		fakeComment.setId(fakeId);
		fakeComment.setText("Updated Fake Comment Text");
		commentDao.update(fakeComment);
		Comment selectedComment = commentDao.selectByKey(fakeId);
		assertEqualsComments(fakeComment, selectedComment);
	}
	
	@Test
	public void testDelete() throws Exception {
		Comment fakeComment = getFakeComment();
		fakeComment.setNewsId(new Long(1));
		Long fakeId = commentDao.create(fakeComment);
		commentDao.delete(fakeId);
		Comment selectedComment = commentDao.selectByKey(fakeId);
		assertNull(selectedComment);
	}

	@Test
	public void testBatchCreate() throws Exception {
		List<Comment> fakeCommentList = getFakeCommentList(10);
		fakeCommentList.forEach(c -> c.setNewsId(new Long(1)));
		List<Long> fakeIdList = commentDao.create(fakeCommentList);
		List<Comment> createdComments = commentDao.selectByKeys(fakeIdList);
		assertEquals(fakeCommentList.size(), createdComments.size());
	}
	
	@Test
	public void testBatchUpdate() throws Exception {
		List<Comment> fakeCommentList = getFakeCommentList(10);
		fakeCommentList.forEach(c -> c.setNewsId(new Long(1)));
		List<Long> fakeIdList = commentDao.create(fakeCommentList);
		setIds(fakeIdList, fakeCommentList);
		fakeCommentList.forEach(c -> c.setText("Updated Comment"));
		commentDao.update(fakeCommentList);
		List<Comment> selectedComments = commentDao.selectByKeys(fakeIdList);
		assertEquals(fakeCommentList.size(), selectedComments.size());
		selectedComments.forEach(c -> assertEquals("Updated Comment", c.getText()));
	}
	
	@Test
	public void testBatchDelete() throws Exception {
		List<Comment> fakeCommentList = getFakeCommentList(10);
		fakeCommentList.forEach(c -> c.setNewsId(new Long(1)));
		List<Long> fakeIdList = commentDao.create(fakeCommentList);
		commentDao.delete(fakeIdList);
		List<Comment> selectedComments = commentDao.selectByKeys(fakeIdList);
		assertTrue(selectedComments.isEmpty());
	}
	
}
