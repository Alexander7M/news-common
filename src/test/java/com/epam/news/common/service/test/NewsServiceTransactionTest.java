package com.epam.news.common.service.test;

import static com.epam.news.common.test.NewsAssertions.assertEquals;
import static com.epam.news.common.test.fakefactory.NewsFakeFactory.getFakeNews;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.epam.news.common.dao.NewsDao;
import com.epam.news.common.dao.exception.DataAccessException;
import com.epam.news.common.dao.test.AppDaoTest;
import com.epam.news.common.entity.News;
import com.epam.news.common.service.ServiceException;
import com.epam.news.common.service.impl.NewsServiceImpl;
import com.epam.news.common.test.NewsAppTestContext;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@DatabaseSetup({"classpath:dataset/input/news.xml", 
	"classpath:dataset/input/author.xml", 
	"classpath:dataset/input/news_author.xml",
	"classpath:dataset/input/tag.xml",
	"classpath:dataset/input/news_tag.xml",
	"classpath:dataset/input/void.xml"})
public class NewsServiceTransactionTest extends AppDaoTest {

	// #TODO = Mockito Annotations
	private NewsDao realNewsDao = NewsAppTestContext.getBean("newsDao", NewsDao.class);
	private NewsDao mockNewsDao = mock(NewsDao.class);
	private NewsServiceImpl newsService = NewsAppTestContext.getBean("newsService", NewsServiceImpl.class); {
		newsService.setNewsDao(mockNewsDao);
	}
	
	private List<News> initialNewsData;
	private List<News> currentNewsData;
	
	@Before
	public void setInitialData() throws Exception {
		initialNewsData = newsService.getAllNews();
	}
	
	public void setCurrentData() throws Exception {
		currentNewsData = newsService.getAllNews();
	}
	
	@After
	public void checkDataTheSame() throws Exception {
		setCurrentData();
		assertEquals(initialNewsData, currentNewsData);
	}
	
	// #TODO = make some exception in service
	@Test(expected = ServiceException.class)
	public void testSaveNews() throws Exception {
		News newsToSave = getFakeNews();
		when(mockNewsDao.create(newsToSave)).then(new Answer<Long>() {
			@Override
			public Long answer(InvocationOnMock invocation) throws Throwable {
				realNewsDao.create(newsToSave);
				throw new DataAccessException();
			}	
		});
		newsService.saveNews(newsToSave);
	}
	
}
