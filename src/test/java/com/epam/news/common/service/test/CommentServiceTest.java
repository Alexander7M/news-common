package com.epam.news.common.service.test;

import static com.epam.news.common.service.util.ListBeanOperations.getIds;
import static com.epam.news.common.test.fakefactory.CommentFakeFactory.getFakeComment;
import static com.epam.news.common.test.fakefactory.CommentFakeFactory.getFakeCommentList;
import static com.epam.news.common.test.fakefactory.CommentFakeFactory.getFakeCommentListWithIds;
import static com.epam.news.common.test.fakefactory.CommentFakeFactory.getFakeCommentWithId;
import static com.epam.news.common.test.fakefactory.IdFakeFactory.getFakeLongId;
import static com.epam.news.common.test.fakefactory.IdFakeFactory.getFakeLongIdList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.news.common.dao.CommentDao;
import com.epam.news.common.entity.Comment;
import com.epam.news.common.service.impl.CommentServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class CommentServiceTest {

	@Mock private CommentDao commentDao;
	@InjectMocks private CommentServiceImpl commentService = new CommentServiceImpl();
	
	@Test
	public void testSaveComment() throws Exception {
		Comment fakeComment = getFakeComment();
		Long fakeId = getFakeLongId();
		when(commentDao.create(fakeComment)).thenReturn(fakeId);
		commentService.saveComment(fakeComment);
		verify(commentDao).create(fakeComment);
		assertEquals(fakeId, fakeComment.getId());
	}
	
	@Test
	public void testUpdateComment() throws Exception {
		Comment fakeComment = getFakeCommentWithId();
		commentService.updateComment(fakeComment);
		verify(commentDao).update(fakeComment);
	}
	
	@Test
	public void testDeleteComment() throws Exception {
		Comment fakeComment = getFakeCommentWithId();
		Long fakeCommentId = fakeComment.getId();
		commentService.deleteComment(fakeComment);
		verify(commentDao).delete(fakeCommentId);
	}
	
	@Test
	public void testSaveComments() throws Exception {
		int fakeSize = 10;
		List<Comment> fakeCommentList = getFakeCommentList(fakeSize);
		List<Long> fakeIdList = getFakeLongIdList(fakeSize);
		when(commentDao.create(fakeCommentList)).thenReturn(fakeIdList);
		commentService.saveComments(fakeCommentList);
		verify(commentDao).create(fakeCommentList);
		assertEquals(fakeIdList, getIds(fakeCommentList));
	}
	
	@Test
	public void testDeleteComments() throws Exception {
		List<Comment> fakeCommentList = getFakeCommentListWithIds(10);
		commentService.deleteComments(fakeCommentList);
		verify(commentDao).delete(getIds(fakeCommentList));
	}
	
}
