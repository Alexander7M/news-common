package com.epam.news.common.service.test;

import static com.epam.news.common.test.NewsAssertions.assertEqualsLinks;
import static com.epam.news.common.test.fakefactory.AuthorFakeFactory.getFakeAuthor;
import static com.epam.news.common.test.fakefactory.AuthorFakeFactory.getFakeAuthorWithId;
import static com.epam.news.common.test.fakefactory.IdFakeFactory.getFakeLongId;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.news.common.dao.AuthorDao;
import com.epam.news.common.dao.exception.DataAccessException;
import com.epam.news.common.entity.Author;
import com.epam.news.common.service.ServiceException;
import com.epam.news.common.service.impl.AuthorServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceTest {

	@Mock private static AuthorDao authorDao;
	@InjectMocks private static AuthorServiceImpl authorService = new AuthorServiceImpl();
	
	@Test
	public void testSaveAuthor() throws Exception {
		Author fakeAuthor = getFakeAuthor();
		Long fakeId = getFakeLongId();
		when(authorDao.create(fakeAuthor)).thenReturn(fakeId);
		authorService.saveAuthor(fakeAuthor);
		verify(authorDao).create(fakeAuthor);
		assertEquals(fakeId, fakeAuthor.getId());
	}
	
	@Test
	public void testDeleteAuthor() throws Exception {
		Author fakeAuthor = getFakeAuthorWithId(getFakeLongId());
		authorService.deleteAuthor(fakeAuthor);
		verify(authorDao).updateExpiredField(fakeAuthor);
		assertNotNull(fakeAuthor.getExpirationDate());
	}
	
	@Test(expected = ServiceException.class)
	public void testDeleteAuthorOnDaoException() throws Exception {
		Author fakeAuthor = getFakeAuthorWithId(getFakeLongId());
		doThrow(new DataAccessException()).when(authorDao).updateExpiredField(fakeAuthor);
		try {
			authorService.deleteAuthor(fakeAuthor);
		} catch (ServiceException e) {
			assertNull(fakeAuthor.getExpirationDate());
			throw e;
		}
	}
	
	@Test
	public void testGetAuthorById() throws Exception {
		Author fakeAuthor = getFakeAuthorWithId();
		Long fakeId = fakeAuthor.getId();
		when(authorDao.selectByKey(fakeId)).thenReturn(fakeAuthor);
		Author selectedAuthor = authorService.getAuthorById(fakeId);
		verify(authorDao).selectByKey(fakeId);
		assertEqualsLinks(selectedAuthor, fakeAuthor);
	}
	
}
