package com.epam.news.common.service.test;

import static com.epam.news.common.service.util.ListBeanOperations.getIds;
import static com.epam.news.common.test.NewsAssertions.assertEqualsLinks;
import static com.epam.news.common.test.fakefactory.AuthorFakeFactory.getFakeAuthorWithId;
import static com.epam.news.common.test.fakefactory.IdFakeFactory.getFakeLongId;
import static com.epam.news.common.test.fakefactory.NewsFakeFactory.getFakeNews;
import static com.epam.news.common.test.fakefactory.NewsFakeFactory.getFakeNewsListWithIds;
import static com.epam.news.common.test.fakefactory.NewsFakeFactory.getFakeNewsWithId;
import static com.epam.news.common.test.fakefactory.TagFakeFactory.getFakeTagList;
import static com.epam.news.common.test.fakefactory.TagFakeFactory.getFakeTagListWithId;
import static com.epam.news.common.test.fakefactory.TagFakeFactory.getFakeTagWithId;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.news.common.dao.NewsDao;
import com.epam.news.common.entity.Author;
import com.epam.news.common.entity.News;
import com.epam.news.common.entity.Tag;
import com.epam.news.common.service.impl.NewsServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class NewsServiceTest {

	@Mock private NewsDao newsDao;
	@InjectMocks private NewsServiceImpl newsService = new NewsServiceImpl();
	
	@Test
	public void testSaveNews() throws Exception {
		News fakeNews = getFakeNews();
		Long fakeId = getFakeLongId();
		when(newsDao.create(fakeNews)).thenReturn(fakeId);
		newsService.saveNews(fakeNews);
		verify(newsDao).create(fakeNews);
		assertEquals(fakeId, fakeNews.getId());
	}
	
	@Test
	public void testUpdateNews() throws Exception {
		News fakeNews = getFakeNewsWithId();
		newsService.updateNews(fakeNews);
		verify(newsDao).update(fakeNews);
	}
	
	@Test
	public void testDeleteNews() throws Exception {
		News fakeNews = getFakeNewsWithId();
		newsService.deleteNews(fakeNews);
		verify(newsDao).delete(fakeNews.getId());
	}
	
	@Test
	public void testGetNewsById() throws Exception {
		News fakeNews = getFakeNewsWithId();
		Long fakeNewsId = fakeNews.getId();
		when(newsDao.selectByKey(fakeNewsId)).thenReturn(fakeNews);
		News selectedNews = newsService.getNewsById(fakeNewsId);
		verify(newsDao).selectByKey(fakeNewsId);
		assertEqualsLinks(fakeNews, selectedNews);
	}
	
	@Test
	public void testGetAllNews() throws Exception {
		List<News> fakeNewsList = getFakeNewsListWithIds(10);
		when(newsDao.selectAllWithCommentCountSorting()).thenReturn(fakeNewsList);
		List<News> selectedNewsList = newsService.getAllNews();
		verify(newsDao).selectAllWithCommentCountSorting();
		assertEqualsLinks(fakeNewsList, selectedNewsList);
	}
	
	@Test
	public void testGetNewsListWritedByAuthor() throws Exception {
		Author fakeAuthor = getFakeAuthorWithId();
		Long fakeAuthorId = fakeAuthor.getId();
		List<News> fakeNewsList = getFakeNewsListWithIds(1);
		when(newsDao.selectByAuthor(fakeAuthorId)).thenReturn(fakeNewsList);
		List<News> selectedNewsList = newsService.getNewsListWritedByAuthor(fakeAuthor);
		verify(newsDao).selectByAuthor(fakeAuthorId);
		assertEqualsLinks(fakeNewsList, selectedNewsList);
	}
	
	@Test
	public void testGetNewsListMarkedByTags() throws Exception {
		List<Tag> fakeTagList = getFakeTagList(2);
		List<Long> fakeTagIdList = getIds(fakeTagList);
		List<News> fakeNewsList = getFakeNewsListWithIds(1);
		when(newsDao.selectByTags(fakeTagIdList)).thenReturn(fakeNewsList);
		List<News> selectedNewsList = newsService.getNewsListMarkedByTags(fakeTagList);
		verify(newsDao).selectByTags(fakeTagIdList);
		assertEqualsLinks(fakeNewsList, selectedNewsList);
	}
	
	@Test
	public void testAddNewsAuthor() throws Exception {
		News fakeNews = getFakeNewsWithId();
		Author fakeAuthor = getFakeAuthorWithId();
		newsService.addNewsAuthor(fakeNews, fakeAuthor);
		verify(newsDao).addNewsAuthor(fakeNews.getId(), fakeAuthor.getId());
	}
	
	@Test
	public void testAddNewsTag() throws Exception {
		News fakeNews = getFakeNewsWithId();
		Tag fakeTag = getFakeTagWithId();
		newsService.addNewsTag(fakeNews, fakeTag);
		verify(newsDao).addNewsTag(fakeNews.getId(), fakeTag.getId());
	}
	
	@Test
	public void testAddNewsTags() throws Exception {
		News fakeNews = getFakeNewsWithId();
		List<Tag> fakeTagList = getFakeTagListWithId(3);
		newsService.addNewsTags(fakeNews, fakeTagList);
		Long fakeNewsId = fakeNews.getId();
		List<Long> fakeTagIdList = getIds(fakeTagList);
		verify(newsDao).addNewsTags(fakeNewsId, fakeTagIdList);
	}
	
}
