package com.epam.news.common.service.test;

import static com.epam.news.common.service.util.ListBeanOperations.getNamesFromTags;
import static com.epam.news.common.test.fakefactory.AuthorFakeFactory.getFakeAuthorWithId;
import static com.epam.news.common.test.fakefactory.IdFakeFactory.getFakeLongId;
import static com.epam.news.common.test.fakefactory.NewsFakeFactory.getFakeNews;
import static com.epam.news.common.test.fakefactory.NewsFakeFactory.getFakeNewsWithId;
import static com.epam.news.common.test.fakefactory.TagFakeFactory.getFakeTagListWithId;
import static com.epam.news.common.test.fakefactory.TagFakeFactory.getFakeTagWithId;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.news.common.entity.Author;
import com.epam.news.common.entity.News;
import com.epam.news.common.entity.Tag;
import com.epam.news.common.service.NewsService;
import com.epam.news.common.service.TagService;
import com.epam.news.common.service.impl.NewsGeneralServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class NewsGeneralServiceTest {

	@Mock private NewsService newsService;
	@Mock private TagService tagService;
	@InjectMocks private NewsGeneralServiceImpl generalService = new NewsGeneralServiceImpl();
	
	@Test
	public void testAddNewsTag() throws Exception {
		News fakeNews = getFakeNewsWithId();
		Tag fakeTag = getFakeTagWithId();
		String fakeTagName = fakeTag.getName();
		when(tagService.getTagByName(fakeTagName)).thenReturn(fakeTag);
		generalService.addNewsTag(fakeNews, fakeTagName);
		verify(newsService).addNewsTag(fakeNews, fakeTag);
	}
	
	@Test
	public void testAddNewsTags() throws Exception {
		News fakeNews = getFakeNewsWithId();
		List<Tag> fakeTagList = getFakeTagListWithId(10);
		List<String> fakeTagNameList = getNamesFromTags(fakeTagList);
		when(tagService.getTagsByNames(fakeTagNameList)).thenReturn(fakeTagList);
		generalService.addNewsTags(fakeNews, fakeTagNameList);
		verify(newsService).addNewsTags(fakeNews, fakeTagList);
	}
	
	@Test
	public void testSaveNewsWithAuthorAndTags() throws Exception {
		News fakeNews = getFakeNews();
		Author fakeAuthor = getFakeAuthorWithId();
		List<Tag> fakeTagList = getFakeTagListWithId(10);
		List<String> fakeTagNameList = getNamesFromTags(fakeTagList);
		when(tagService.getTagsByNames(fakeTagNameList)).thenReturn(fakeTagList);
		doAnswer(new Answer<Void>() {
			@Override
			public Void answer(InvocationOnMock invocation) throws Throwable {
				Long fakeNewsId = getFakeLongId();
				fakeNews.setId(fakeNewsId);
				return null;
			}
		}).when(newsService).saveNews(fakeNews);
		generalService.saveNewsWithAuthorAndTags(fakeNews, fakeAuthor, fakeTagNameList);
		verify(newsService).saveNews(fakeNews);
		verify(tagService).getTagsByNames(fakeTagNameList);
		verify(newsService).addNewsTags(fakeNews, fakeTagList);
		verify(newsService).addNewsAuthor(fakeNews, fakeAuthor);
	}
	
}
