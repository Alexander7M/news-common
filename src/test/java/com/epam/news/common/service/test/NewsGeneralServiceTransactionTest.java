package com.epam.news.common.service.test;

import static com.epam.news.common.test.NewsAssertions.assertEquals;
import static com.epam.news.common.test.fakefactory.NewsFakeFactory.getFakeNews;
import static com.epam.news.common.test.fakefactory.TagFakeFactory.getFakeTagNameList;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.epam.news.common.dao.test.AppDaoTest;
import com.epam.news.common.entity.Author;
import com.epam.news.common.entity.News;
import com.epam.news.common.service.NewsGeneralService;
import com.epam.news.common.service.NewsService;
import com.epam.news.common.service.ServiceException;
import com.epam.news.common.test.NewsAppTestContext;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@DatabaseSetup({"classpath:dataset/input/news.xml", 
	"classpath:dataset/input/author.xml", 
	"classpath:dataset/input/news_author.xml",
	"classpath:dataset/input/tag.xml",
	"classpath:dataset/input/news_tag.xml",
	"classpath:dataset/input/void.xml"})
public class NewsGeneralServiceTransactionTest extends AppDaoTest {

	private NewsService newsService = NewsAppTestContext.getBean("newsService", NewsService.class);
	private NewsGeneralService newsGeneralService = NewsAppTestContext.getBean("newsGeneralService", NewsGeneralService.class);
	
	private List<News> initialNewsData;
	private List<News> currentNewsData;
	
	@Before
	public void setInitialData() throws Exception {
		initialNewsData = newsService.getAllNews();
	}
	
	public void setCurrentData() throws Exception {
		currentNewsData = newsService.getAllNews();
	}
	
	@After
	public void checkDataTheSame() throws Exception {
		setCurrentData();
		assertEquals(initialNewsData, currentNewsData);
	}
	
	@Test(expected = ServiceException.class)
	public void testSaveNewsWithAuthorAndTags() throws ServiceException {
		News newsToSave = getFakeNews(); /* just some news to be saved */
		Author newsAuthor = new Author(new Long(1)); /* real author in database */
		List<String> fakeTagNameList = getFakeTagNameList(10); /* this tags is not exist => exception will be thrown in servise */
		newsGeneralService.saveNewsWithAuthorAndTags(newsToSave, newsAuthor, fakeTagNameList); /* expecting exception and rollback */
	}
	
}
