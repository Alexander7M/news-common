package com.epam.news.common.service.test;

import static com.epam.news.common.service.util.ListBeanOperations.getIds;
import static com.epam.news.common.service.util.ListBeanOperations.getNamesFromTags;
import static com.epam.news.common.test.fakefactory.IdFakeFactory.getFakeLongId;
import static com.epam.news.common.test.fakefactory.IdFakeFactory.getFakeLongIdList;
import static com.epam.news.common.test.fakefactory.TagFakeFactory.getFakeTag;
import static com.epam.news.common.test.fakefactory.TagFakeFactory.getFakeTagList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.news.common.dao.TagDao;
import com.epam.news.common.entity.Tag;
import com.epam.news.common.service.impl.TagServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceTest {

	@Mock private TagDao tagDao;
	@InjectMocks private TagServiceImpl tagService = new TagServiceImpl();
	
	@Test
	public void testCreateTag() throws Exception {
		Tag fakeTag = getFakeTag();
		Long fakeId = getFakeLongId();
		when(tagDao.create(fakeTag)).thenReturn(fakeId);
		tagService.createTag(fakeTag);
		verify(tagDao).create(fakeTag);
		assertEquals(fakeId, fakeTag.getId());
	}
	
	@Test
	public void testGetTagByName() throws Exception {
		Tag fakeTag = getFakeTag();
		String fakeTagName = fakeTag.getName();
		when(tagDao.selectByName(fakeTagName)).thenReturn(fakeTag);
		Tag selectedTag = tagService.getTagByName(fakeTagName);
		verify(tagDao).selectByName(fakeTagName);
		assertEquals(fakeTag, selectedTag);
	}
	
	@Test
	public void testCreateTags() throws Exception {
		int fakeListSize = 10;
		List<Tag> fakeTagList = getFakeTagList(fakeListSize);
		List<Long> fakeIdList = getFakeLongIdList(fakeListSize);
		when(tagDao.create(fakeTagList)).thenReturn(fakeIdList);
		tagService.createTags(fakeTagList);
		verify(tagDao).create(fakeTagList);
		assertEquals(fakeIdList, getIds(fakeTagList));
	}
	
	@Test
	public void testGetTagsByNames() throws Exception {
		int fakeListSize = 10;
		List<Tag> fakeTagList = getFakeTagList(fakeListSize);
		List<String> fakeTagNameList = getNamesFromTags(fakeTagList);
		when(tagDao.selectByNames(fakeTagNameList)).thenReturn(fakeTagList);
		List<Tag> selectedTags = tagService.getTagsByNames(fakeTagNameList);
		verify(tagDao).selectByNames(fakeTagNameList);
		assertEquals(selectedTags, fakeTagList);
	}
	
}
