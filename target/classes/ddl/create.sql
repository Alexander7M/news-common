--------------------------------------------------------
--  DDL for Droping Tables
--------------------------------------------------------
DROP TABLE "COMMENTS";
DROP TABLE "NEWS_AUTHOR";
DROP TABLE "AUTHOR";
DROP TABLE "NEWS_TAG";
DROP TABLE "TAG";
DROP TABLE "NEWS";
DROP TABLE "ROLES";
DROP TABLE "USER";
--------------------------------------------------------
--  DDL for Droping Sequences
--------------------------------------------------------
DROP SEQUENCE "AUTHOR_AUTHOR_ID_SEQ";
DROP SEQUENCE "COMMENTS_COMMENT_ID_SEQ";
DROP SEQUENCE "NEWS_NEWS_ID_SEQ";
DROP SEQUENCE "TAG_TAG_ID_SEQ";
DROP SEQUENCE "USER_USER_ID_SEQ";
--------------------------------------------------------
--  DDL for Droping Views
--------------------------------------------------------
DROP VIEW "NEWS_TAG_NAME";
--------------------------------------------------------
--  DDL for Creating Sequences
--------------------------------------------------------
CREATE SEQUENCE "AUTHOR_AUTHOR_ID_SEQ" START WITH 1 INCREMENT BY 1 MAXVALUE 18446744073709551615 NOCACHE NOORDER NOCYCLE;
CREATE SEQUENCE "COMMENTS_COMMENT_ID_SEQ" START WITH 1 INCREMENT BY 1 MAXVALUE 18446744073709551615 NOCACHE NOORDER NOCYCLE;
CREATE SEQUENCE "NEWS_NEWS_ID_SEQ" START WITH 1 INCREMENT BY 1 MAXVALUE 18446744073709551615 NOCACHE NOORDER NOCYCLE;
CREATE SEQUENCE "TAG_TAG_ID_SEQ" START WITH 1 INCREMENT BY 1 MAXVALUE 18446744073709551615 NOCACHE NOORDER NOCYCLE;
CREATE SEQUENCE "USER_USER_ID_SEQ" START WITH 1 INCREMENT BY 1 MAXVALUE 18446744073709551615 NOCACHE NOORDER NOCYCLE;
--------------------------------------------------------
--  DDL for Creating Tables
--------------------------------------------------------
CREATE TABLE "AUTHOR" (
  "AUTHOR_ID" NUMBER(20) PRIMARY KEY NOT NULL,
  "AUTHOR_NAME" NVARCHAR2(30) NOT NULL,
  "EXPIRED" TIMESTAMP(6)
);
--------------------------------------------------------
CREATE TABLE "NEWS" (
  "NEWS_ID" NUMBER(20) PRIMARY KEY NOT NULL, 
  "SHORT_TEXT" NVARCHAR2(200) NOT NULL, 
  "FULL_TEXT" NVARCHAR2(2000) NOT NULL, 
  "TITLE" NVARCHAR2(60) NOT NULL, 
  "CREATION_DATE" TIMESTAMP(6) DEFAULT LOCALTIMESTAMP NOT NULL, 
  "MODIFICATION_DATE" DATE DEFAULT SYSDATE NOT NULL
);
--------------------------------------------------------
CREATE TABLE "NEWS_AUTHOR" (
  "AUTHOR_ID" NUMBER(20) REFERENCES "AUTHOR"("AUTHOR_ID") NOT NULL, 
  "NEWS_ID" NUMBER(20) REFERENCES "NEWS"("NEWS_ID") NOT NULL
);
--------------------------------------------------------
CREATE TABLE "TAG" (
  "TAG_ID" NUMBER(20) PRIMARY KEY NOT NULL, 
  "TAG_NAME" NVARCHAR2(30) NOT NULL
);
--------------------------------------------------------
CREATE TABLE "NEWS_TAG" (
  "NEWS_ID" NUMBER(20) REFERENCES "NEWS"("NEWS_ID") NOT NULL, 
  "TAG_ID" NUMBER(20) REFERENCES "TAG"("TAG_ID") NOT NULL
);
--------------------------------------------------------
CREATE TABLE "COMMENTS" (
  "COMMENT_ID" NUMBER(20) PRIMARY KEY NOT NULL, 
  "COMMENT_TEXT" NVARCHAR2(100) NOT NULL, 
  "CREATION_DATE" TIMESTAMP(6) DEFAULT LOCALTIMESTAMP NOT NULL, 
  "NEWS_ID" NUMBER(20) REFERENCES "NEWS"("NEWS_ID") NOT NULL
);
--------------------------------------------------------
CREATE TABLE "USER" (
  "USER_ID" NUMBER(20) PRIMARY KEY NOT NULL,
  "USER_NAME" NVARCHAR2(50) NOT NULL,
  "LOGIN" VARCHAR2(30) NOT NULL,
  "PASSWORD" VARCHAR2(30) NOT NULL
);
--------------------------------------------------------
CREATE TABLE "ROLES" (
  "USER_ID" NUMBER(20) REFERENCES "USER"("USER_ID") NOT NULL,
  "ROLE_NAME" VARCHAR2(50) NOT NULL
);




--------------------------------------------------------
--  DDL for Creating Views
--------------------------------------------------------
CREATE OR REPLACE FORCE VIEW "NEWS_TAG_NAME" ("NEWS_ID", "TAG_NAME")
AS SELECT "NEWS_TAG"."NEWS_ID", "TAG"."TAG_NAME"
FROM "NEWS_TAG" INNER JOIN "TAG" ON "NEWS_TAG"."TAG_ID" = "TAG"."TAG_ID";





--------------------------------------------------------
--  DDL for Trigger AUTHOR_AUTHOR_ID_TRG
--------------------------------------------------------
CREATE OR REPLACE TRIGGER "AUTHOR_AUTHOR_ID_TRG" 
BEFORE INSERT ON "AUTHOR"
FOR EACH ROW
WHEN (NEW.AUTHOR_ID IS NULL)
BEGIN 
  :NEW.author_id := Author_author_id_SEQ.NEXTVAL;
END;
/
ALTER TRIGGER "AUTHOR_AUTHOR_ID_TRG" ENABLE;
--------------------------------------------------------
--  DDL for Trigger COMMENTS_COMMENT_ID_TRG
--------------------------------------------------------
CREATE OR REPLACE TRIGGER "COMMENTS_COMMENT_ID_TRG"
BEFORE INSERT ON Comments
FOR EACH ROW
WHEN (NEW.comment_id IS NULL)
BEGIN 
  :NEW.comment_id := Comments_comment_id_SEQ.NEXTVAL;
END;
/
ALTER TRIGGER "COMMENTS_COMMENT_ID_TRG" ENABLE;
--------------------------------------------------------
--  DDL for Trigger NEWS_NEWS_ID_TRG
--------------------------------------------------------
CREATE OR REPLACE TRIGGER "NEWS_NEWS_ID_TRG" 
BEFORE INSERT ON "NEWS" 
FOR EACH ROW  
WHEN (NEW.news_id IS NULL) 
BEGIN 
  :NEW.news_id := News_news_id_SEQ.NEXTVAL;
END;
/
ALTER TRIGGER "NEWS_NEWS_ID_TRG" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TAG_TAG_ID_TRG
--------------------------------------------------------
CREATE OR REPLACE TRIGGER "TAG_TAG_ID_TRG"
BEFORE INSERT ON Tag 
FOR EACH ROW  
WHEN (NEW.tag_id IS NULL) 
BEGIN 
  :NEW.tag_id := Tag_tag_id_SEQ.NEXTVAL;
END;
/
ALTER TRIGGER "TAG_TAG_ID_TRG" ENABLE;
--------------------------------------------------------
--  DDL for Trigger USER_USER_ID_TRG
--------------------------------------------------------
CREATE OR REPLACE TRIGGER "USER_USER_ID_TRG"
BEFORE INSERT ON "USER"
FOR EACH ROW
WHEN (NEW.USER_ID IS NULL) 
BEGIN 
  :NEW.USER_ID := "USER_USER_ID_SEQ".NEXTVAL;
END;
/
ALTER TRIGGER "USER_USER_ID_TRG" ENABLE;
--------------------------------------------------------
--  DDL for Trigger INSERT_INTO_NEWS_TAG_NAME_TRG
--------------------------------------------------------
CREATE OR REPLACE TRIGGER "INSERT_INTO_NEWS_TAG_NAME_TRG" 
instead of insert on news_tag_name
for each row
declare
  ins_tag_id NUMBER(20);
begin
  select tag_id into ins_tag_id from tag where tag_name = :new.tag_name;
  insert into news_tag (news_id, tag_id) values (:new.news_id, ins_tag_id);
  exception
    when no_data_found then
      insert into tag (tag_name) values (:new.tag_name)
      returning tag_id into ins_tag_id;
      insert into news_tag (news_id, tag_id) values (:new.news_id, ins_tag_id);
end insert_into_news_tag_name_trg;
/
ALTER TRIGGER "INSERT_INTO_NEWS_TAG_NAME_TRG" ENABLE;