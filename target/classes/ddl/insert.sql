
Insert into NEWS.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (1,'It�s been a mystery for 80 years�but gamma rays from a dwarf galaxy could finally tell scientists what dark matter is really made of.','It�s too early to say for certain, but astronomers may have picked up a new clue to the nature of dark matter�invisible cosmic stuff with at least five times the mass of all the visible stars and galaxies combined.

The clue comes in the form of gamma rays, a kind of light the human eye can�t detect, emanating from a newly discovered dwarf galaxy called Reticulum 2. Reticulum 2, which hovers beyond the edge of the Milky Way, about 98,000 light-years from Earth, is fascinating in its own right: No more than a few thousand stars (compared with the Milky Way�s hundred billion or more) embedded in a clump of dark matter, it�s similar to the first tiny galaxies that appeared after the big bang. (See �Galaxy Hunters�The Search for Cosmic Dawn� to learn more about the earliest galaxies.)

The mystery of dark matter dates back all the way to the 1930s, when the legendary astronomer Fritz Zwicky first noticed that galaxies in clusters seemed to be moving under the gravity of some strange, unseen substance. But so far nobody has figured out what it really is.  (To learn more about dark matter, see "A First Hint of the Hidden Cosmos" in National Geographic magazine.)

Reticulum 2, along with eight other recently discovered dwarf galaxies, are intriguing for what they can tell scientists about what the early universe might have looked like. But they�re also prime places to try and figure out what dark matter is, because they have more dark matter compared to visible matter than a full-size galaxy such as the Milky Way does.','Have Astronomers Finally Found Dark Matter?',to_timestamp('16-MAR-15 06.30.30.020000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('16-MAR-15','DD-MON-RR'));
Insert into NEWS.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (2,'According to photographer Drew Rush, all it takes to capture the right bear picture is a camera, a trigger, and a "bit of disappointment."','Fall was in the air that September, and the grizzly bears I was attempting to photograph in the Beartooth Mountains were hungry, getting ready for their long winter�s sleep. A week had already gone by since the last time I had gotten anything close to a useable picture.

Waking up, making coffee, and telling myself that today would be the day I got the frame was my routine. Then out into the field to fix my camera traps, hiking from one to the next, and repeat.

I walked along the edge of the lake to check on a recently placed trap, thinking about all of the problems that a bear creates as it raids stashes of pinecones buried by red squirrels for their winter reserves of food. A bear gorging itself on pine nuts doesn�t think twice about my tiny little camera that just might be in its way, and more often than not I would show up to check a site and find my camera traps destroyed by the ravenous bears.

Working with camera traps is a lesson in patience and guts. You have to be able to tell yourself that eventually it�s going to happen, and you have to have to perseverance to fix repeated problems while knowing that in all likelihood those problems are going to happen again.

At its simplest, the setup consists of a camera, a trigger, and the promise of disappointment. You do everything you can to make sure the camera box is securely positioned, that the animal will trigger the camera exactly where you expect it to, that the lighting you have meticulously placed to freeze the movement of feeding bears is perfect�only to find that either nothing had walked through it, a squirrel had chewed the cables, a dead branch had fallen and knocked over the camera box, or, as was happening most often, the traps had been destroyed by ravenous bears�and you didn�t get any pictures.','Looking for Hungry Grizzlies�on Purpose',to_timestamp('16-MAR-15 06.37.57.177000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('16-MAR-15','DD-MON-RR'));
Insert into NEWS.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (3,'Why does a number describing the ratio between a circle''s circumference and its diameter inspire such devotion?','Number geeks�and pie aficionados�around the world have even more reason to rejoice this year when they celebrate Pi Day. That''s because March 14, 2015, allows them to celebrate their beloved number out to four decimal places (3.1415), instead of two (3.14).

The truly precise can raise a toast at 9:26 a.m. to add a few more digits. This bit of calendar luck happens just once a century, so the next will be March 14, 2115.

Pi is a circle''s circumference�the distance around it�divided by its diameter, or the distance through its center. The relationship holds no matter the size of the circle: The rim of a soup can and a planet''s equator both yield a ratio of pi. (See pictures of circles in nature.)

This rather simple-sounding description belies some rather fascinating qualities, says Ron Hipschman at the Exploratorium in San Francisco. Museum staff held the first Pi Day celebration in 1988 by eating pie, and have carried on the tradition every year since, adding a Pi Day "shrine" and a procession. (Read about what they did when Pi Day turned 25.)

"Pi is interesting because it goes on forever [and its sequence] never repeats," Hipschman says. That''s probably part of the reason why people are so enamored of it, he adds.','Pi Day of the Century: Celebrating an Irrational Number',to_timestamp('16-MAR-15 06.40.14.921000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('16-MAR-15','DD-MON-RR'));
Insert into NEWS.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (4,'The Soyuz spacecraft returned to Earth carrying an American astronaut and two Russian cosmonauts.','We�re used to astronauts returning to Earth in reentry vehicles on a runway or bobbing in the ocean in their capsule. Here''s something you probably haven''t seen before�the sight of astronauts tethered to a beautiful golden parachute as they landed shortly after sunrise.

Two Russian cosmonauts and an American astronaut returned safely to Earth after spending nearly six months at the International Space Station. Their Soyuz spacecraft landed on March 12 shortly after 5:00 a.m. Moscow time (2:00 GMT).

NASA photographer Bill Ingalls captured the moments in a series of photographs highlighted by an especially beautiful image showing the space capsule as it parachuted slowly to Earth above the clouds in early morning light (see above).

NASA said their 167-day mission included hundreds of scientific experiments and several spacewalks. Their research focused on the effects of microgravity on cells, Earth observation, and physical, biological, and molecular science.

The space scientists also focused on human health impacts from long-term space travel. NASA said two new crew members will soon spend a year on the space station.

NASASpaceFlight.com reported that the landing was complicated by a brief loss of radio communications with the spacecraft as it neared the Earth.

Dennis Dimick is a photo editor and National Geographic''s Executive Editor for the Environment. You can find him on Twitter, Instagram, and flickr.','Space Station Astronauts Float to Earth in Golden Parachute',to_timestamp('16-MAR-15 06.55.45.286000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('16-MAR-15','DD-MON-RR'));
Insert into NEWS.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (5,'Particles spewed from its ice-encrusted ocean hint at the same kind of chemistry that might have jump-started Earth�s biology.','Saturn�s icy moon Enceladus has tantalized scientists for years as a place where life could exist (in theory), and the case is now stronger with new evidence of hot springs beneath the moon''s icy crust.

The evidence, reported Wednesday in the journal Nature, comes in the form of icy, salty dust grains erupting from an ocean under the moon�s frozen crust. The chemical nature of those grains suggests the same sort of environment, writes Gabriel Tobie of the University of Nantes, France, in an accompanying Nature commentary, that �might have been the birthplace of the first living organisms on Earth.�

The dust grains were discovered several years ago in Saturn�s E ring, its largest ring, which planetary scientists believe is fed by material from geysers that spew from cracks in the surface of Enceladus. The geysers are fed in turn by the subsurface ocean, whose long-suspected presence was confirmed by the Cassini spacecraft last year.

NASA sees moons as promising targets in the hunt for alien life, and is in the early stages of planning a robotic mission to explore the ice-covered ocean on Jupiter''s moon Europa. If they can work out the technology to crack through Europa''s ice and look for signs of habitability or even life, Enceladus might be next.

This new research reveals that some of the dust comes in the form of tiny particles just a few hundred-millionths of an inch across and rich in silicon dioxide. �We think,� says lead author Hsiang-Wen Hsu, of the University of Colorado, Boulder, �it indicates ongoing hydrothermal activity��that is, interactions between water and rock.

If so, it could be a crucial discovery. Water in liquid form is considered essential to life by astrobiologists. So is a supply of complex, carbon-based compounds, which could easily have come to Enceladus riding on comets. (See "The Hunt for Life Beyond Earth.")','Saturn Moon Just Got More Interesting in Hunt for Alien Life',to_timestamp('16-MAR-15 06.58.53.988000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('16-MAR-15','DD-MON-RR'));
Insert into NEWS.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (6,'Discovery shows that quadruple-star systems may be more common than previously thought.','The Star Wars world of Tatooine has nothing on a distant planet now known to have a whopping four suns.  

Only the second known quadruple-star system found with a planet, 30 Ari hosts a gaseous super-Jupiter estimated to have ten times the mass of our solar system''s largest planet. The behemoth orbits its primary star every 335 days but lies 23 times as distant from the newfound fourth star as Earth is from the sun, researchers reported March 4 in the Astronomical Journal.

Located 136 light-years away in the constellation Aries, 30 Ari was thought to be a triple-star system when its planet was discovered in 2009. The faint fourth star was found using new adaptive optics technologies attached to the Palomar Observatory telescope in California. The adaptive optics technique cancels out the blurring effect of Earth''s atmosphere and provides crystal clear views of distant objects in space.

Multiple-star systems are more common than singletons like our sun. But the discovery of a fourth star in the already crowded 30 Ari suggests that systems with four or more stars may be more common across the Milky Way than astronomers had once believed.','Faraway Planet Gains a Fourth Sun',to_timestamp('16-MAR-15 07.01.42.044000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('16-MAR-15','DD-MON-RR'));
Insert into NEWS.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (7,'Female''s trek nails down how wolves get to the Lake Superior island home of a controversial, and dwindling, wolf pack.','For the first time, a wolf has been tracked crossing an ice bridge into northern Michigan''s Isle Royale National Park�an impressive feat that might be a hopeful sign for the predator''s ability to survive climate change.

On February 22, the GPS-collared wolf left her territory in eastern Minnesota''s Chippewa Grand Portage Reservation and trotted 14 miles (23 kilometers)�the shortest possible distance to her destination�across the rugged Lake Superior ice to Isle Royale, a remote, forested island that''s home to an unrelated wolf pack. 

An uncollared companion of unknown gender came with her; the pair stayed on the island a few days before returning home on February 27, according to GPS data collected by Seth Moore, director of biology and environment for the Grand Portage Band of Chippewa.

"It''s cool on a bunch of different levels. First of all, it establishes conclusively how wolves get to Isle Royale�this was all speculation up until last week," says Moore.

Scientists have long predicted that wolves get to the island via ice bridges in Lake Superior. Since these bridges don''t form reliably anymore thanks to rising temperatures in the region, some believe the future of Isle Royale wolves is tenuous at best.

At the same time, the island wolf population has been steadily declining, according to scientists with the Wolves and Moose of Isle Royale Project. In 2009, scientists documented about 24 wolves living on the island. By February 2014, that population had dwindled to nine, says Rolf Peterson, a wildlife ecologist at Michigan Technological University in Houghton who has led the project since the 1970s.

The likely reason for the decline is inbreeding. With few outside wolves breeding with the island population, and therefore little genetic diversity among the remaining wolves, all the animals have skeletal deformities, and their weakened state could be interfering with reproduction.','How Do Wolves Get to a Remote Island? Ice Solves Mystery',to_timestamp('16-MAR-15 07.03.27.339000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('16-MAR-15','DD-MON-RR'));
Insert into NEWS.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (8,'With a little help, and an early morning rise, the rings of Saturn may be seen.','Late winter stars and planets take center stage this week, giving plenty of celestial reasons to go outside in both evening and morning.

Pyramid of Light. The next couple of weeks will provide ample opportunity to catch the mysterious zodiacal light.

The pyramid-shaped beam of light, also known as false dawn or false dusk, can easily be mistaken for the lights of a far-off city. But the light is more ethereal, and its source much more distant. Zodiacal light is caused by sunlight reflecting off billions of dustlike particles in the inner solar system that were left over after the planets formed about 4.6 billion years ago.

On Monday, March 9, and for the rest of the month, keen viewers in the Northern Hemisphere can hunt down this most elusive of astronomical phenomena. The best time to catch the ghostly skylight is about an hour after sunset, looking toward the western horizon from the dark countryside.

Ganymede Gains on Jupiter. Later the same night, sky-watchers are treated to the sight of Ganymede crossing the face of Jupiter. The moon�the seventh, and largest, in the Jovian system�will start its transit at 10 p.m. eastern daylight time, and the event will last for about three and a half hours. The moon''s shadow trail, however, will lag behind a whole three hours, extending the show.

Galileo discovered Ganymede in 1610. The moon, which is about 8 percent larger than the planet Mercury, is now known to be the largest in the solar system. Scientists suspect that Ganymede, composed of rock and ice, may hold oceans capable of supporting life.','Watch the Night Sky This Week for Eerie Light, a Giant Moon',to_timestamp('16-MAR-15 07.06.14.003000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('16-MAR-15','DD-MON-RR'));
Insert into NEWS.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (9,'High-tech tools, including an undersea "mountain goat," and years of research led to the discovery of the WWII-era Musashi in the Pacific.','After years of meticulous historical research and seafloor terrain analysis, it was an underwater "mountain goat" that ultimately found the wreck of one of history''s most impressive battleships, the Musashi.

Researchers led by Microsoft co-founder Paul Allen, aboard Allen''s motor yacht, the M.Y. Octopus, announced that they had located the imperial Japanese Navy battleship at a depth of approximately 3,280 feet (one kilometer) in Philippine waters on March 2. Japanese naval historian Kazushige Todaka confirmed its identification.

The 73,000-ton (66,224 metric tons) Musashi and sister ship Yamato were the largest battleships the world has ever known. Allied forces sunk the Musashi on October 24, 1944, during the Battle of Leyte Gulf, considered the largest naval battle of World War II and quite possibly the largest naval battle in history. Almost half of the Musashi''s 2,399-man crew perished.

Work to determine the search area for the wreck began more than eight years ago with an analysis of primary sources, which list four different sinking positions in the Sibuyan Sea: the "official" Japanese and U.S. Navy positions, one recorded in the log of a Japanese destroyer standing by to rescue the Musashi''s crew, and a drawing by a Japanese survivor of the battle indicating where the ship sank in relation to Sibuyan Island.

By augmenting these accounts with dozens of other navigational clues, the team was able to identify an overall search area of 360 square nautical miles (477 square miles, or 1,236 square kilometers), according to David Mearns of Bluewater Recoveries, who assisted with research and determining the final search area.

Side-scan sonar, which can detect features or objects rising from the seafloor, is commonly used to identify potential shipwrecks. The sonar sensor is usually towed behind a survey vessel at a consistent depth, but an early attempt was hampered by long fishing lines entangling the sensor.','How Microsoft Billionaire Found Largest Sunken Battleship',to_timestamp('16-MAR-15 07.07.46.428000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('16-MAR-15','DD-MON-RR'));
Insert into NEWS.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (10,'With a little help, and an early morning rise, the rings of Saturn may be seen.','Late winter stars and planets take center stage this week, giving plenty of celestial reasons to go outside in both evening and morning.

Pyramid of Light. The next couple of weeks will provide ample opportunity to catch the mysterious zodiacal light.

The pyramid-shaped beam of light, also known as false dawn or false dusk, can easily be mistaken for the lights of a far-off city. But the light is more ethereal, and its source much more distant. Zodiacal light is caused by sunlight reflecting off billions of dustlike particles in the inner solar system that were left over after the planets formed about 4.6 billion years ago.

On Monday, March 9, and for the rest of the month, keen viewers in the Northern Hemisphere can hunt down this most elusive of astronomical phenomena. The best time to catch the ghostly skylight is about an hour after sunset, looking toward the western horizon from the dark countryside.

Ganymede Gains on Jupiter. Later the same night, sky-watchers are treated to the sight of Ganymede crossing the face of Jupiter. The moon�the seventh, and largest, in the Jovian system�will start its transit at 10 p.m. eastern daylight time, and the event will last for about three and a half hours. The moon''s shadow trail, however, will lag behind a whole three hours, extending the show.

Galileo discovered Ganymede in 1610. The moon, which is about 8 percent larger than the planet Mercury, is now known to be the largest in the solar system. Scientists suspect that Ganymede, composed of rock and ice, may hold oceans capable of supporting life.','Watch the Night Sky This Week for Eerie Light, a Giant Moon',to_timestamp('16-MAR-15 07.09.21.669000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('16-MAR-15','DD-MON-RR'));
Insert into NEWS.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (11,'Swiss pilots hope to complete journey in five months.','Little more than a century after the Wright brothers launched a gas-powered airplane from a sandhill outside Kitty Hawk, a team of Swiss explorers took off Sunday for their own run at aviation history, in a plane fueled only by sunlight.

Strapped into the cramped, single-seat cockpit of a plane called the Solar Impulse 2, former fighter pilot Andr? Borschberg lifted off just after 11 p.m. EDT Sunday from a military airport near the Strait of Hormuz and began the first leg in a quest to fly a solar-powered craft around the world.

Borschberg will head first to Muscat, Oman, gradually climbing toward 28,000 feet (8,500 meters) as a vast array of solar panels stretched across the wing tops and fuselage drink in sunlight and power the craft�s four electric engines. After Oman the plane will continue east, visiting cities in India and China before attempting to reach Hawaii on a perilous, multi-day crossing of the Pacific.

The journey is expected to last five months and cover some 21,000 miles (33,800 kilometers), with frequent stops along the way where Borschberg will trade off flying duties with his partner-in-adventure, Bertrand Piccard. If their circumnavigation is successful, the men will be the first to break the fuel barrier�crossing immense distances, flying day and night, without consuming a drop of gasoline.

�This is the future,� said Piccard, a psychiatrist and adventurer who in 1999 was first to fly around the world in a gas balloon. �It�s another way to think, to fly, to promote sustainability. We�ve come a long way since the Wright brothers.�
','Solar Plane Takes Off on Round-the-World Quest',to_timestamp('16-MAR-15 07.10.57.813000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('16-MAR-15','DD-MON-RR'));
Insert into NEWS.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (12,'One of the things grief does is shatter the narrative of your life, says author.','Helen Macdonald was at home in Cambridge, England, when she got a phone call saying her father, Alisdair, had died suddenly of a heart attack on a London street. The news shattered her world, propelling her into a vortex of raw grief.

As she struggled to come to terms with her father''s loss, she began to have dreams about goshawks, the wildest, most temperamental of the hawk family. An experienced falconer since childhood, she decided to buy and train one. Her memoir of that experience, H Is for Hawk, must be one of the most riveting encounters between a human being and an animal ever written. 

Talking from her home near Newmarket, England, Macdonald describes why Hermann G?ring loved hawks, what links the Turkish word for penis with a hawk''s ideal flying weight, and how training a goshawk took her to the edge of madness but eventually gave her peace�and a new kinship with other people.

You embarked on the training of a goshawk because of a sudden, personal loss. Can you talk about it?

I don''t recommend that people try to train goshawks as a way of managing grief. [Laughs] It''s not a brilliant way of going about it. But in March 2007, my very beloved father was out taking photographs of storm-damaged buildings for the Evening Standard in London when he had a massive heart attack and died.

We didn''t know he had heart problems of any kind, so it was a huge and horrible surprise. As anyone who''s experienced loss will know, the first few weeks you''re in a total daze. You go to a funeral, you organize things, but you don''t really know what''s going on.

I went back to Cambridge where I was teaching and thought, everything''s fine, I''ll just get on with it. But I started dreaming about goshawks. Some part of me that wasn''t available to me consciously started to really tell me that I needed to train a hawk.
','How Training a Wild Hawk Healed One Woman''s Broken Heart',to_timestamp('16-MAR-15 07.12.31.177000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('16-MAR-15','DD-MON-RR'));
Insert into NEWS.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (13,'The giant fish, which help keep coral reefs healthy, have declined due to their popularity as seafood.','Love the beach? You can thank the bumphead parrotfish, Bolbometopon muricatum, a wide-ranging species that eats massive amounts of coral and, well, poops it out as that luxurious, toe-wriggle-worthy sand.

The predators'' constant pruning keeps reefs from getting overcrowded and prevents the growth of weedy, invasive corals that can smother it. (See "As Oceans Heat Up, a Race to Save World''s Coral Reefs.")

Now, in an effort to help save these valuable fish, a study has unveiled the giants'' unusual mating habits�such as head-butting over territory.

Despite its crucial role in keeping coral reefs healthy�and beachgoers happy�the greenish-blue predator is in trouble, in part because it''s also a popular dinner entree. (See "Predator Fish Help Coral Reefs Rebound, Study Shows.")

In all of the regions where its Pacific and Indian Ocean habitat overlaps with human activities, the 165-pound (75-kilogram) parrotfish�the world''s largest�has been devastated by overfishing. Not only is it considered good eating, the animal''s habit of sleeping in large, shallow-water schools makes it an easy target for fishermen. As a result, the International Union for Conservation of Nature lists the bumphead parrotfish as vulnerable to extinction.

But the fish with the funny forehead has found a few coral oases where humans don''t reach, including parts of the Great Barrier Reef and Wake Atoll (map), a remote U.S. marine reserve in the western Pacific. (See pictures of protected ocean areas in the U.S.)','Head-Bashing and Other Mating Secrets of Giant Parrotfish',to_timestamp('16-MAR-15 07.13.40.887000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('16-MAR-15','DD-MON-RR'));
Insert into NEWS.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (14,'Colors give butterflies camouflage, which helps them avoid hungry predators.','Ask a social butterfly where she got that great dress, and she''ll say, "This old thing?" and then tell you its entire history.

Ask an actual butterfly about its colorful attire, and things get a lot more complicated.

Our Weird Animal Question of the Week comes to us from National Geographic''s own Angie McPherson, a volunteer at the Smithsonian Butterfly Garden in Washington, D.C.''s National Museum of Natural History. She asked, "Why does the paper kite butterfly create a gold chrysalis?" (See "New Golden Bat Adds to Animals With the Midas Touch.")

The paper kite butterfly, native to Asia, is light yellow or off-white with an elaborate pattern of swooping black lines and dots. But its chrysalis�a hard case that protects the caterpillar during its final transformation into a butterfly�is a shiny, golden hue.

It''s unknown why the chrysalis itself is gold, but its shininess helps camouflage the developing butterfly, says Katy Prudic, a biologist at Oregon State University in Corvallis.

In particular, the sheen is "disruptive" to potential predators�it makes the chrysalis "hard to detect in a complicated background," Prudic says. A hungry bird may even think it looks like a drop of water.

"Sitting Duck"

Camouflage is crucial to chrysalides: Because growing butterflies are unable to move and in danger of being eaten or parasitized, "they''re a sitting duck," Prudic notes.

The giant swallowtail is another example of chrysalis camo. In that species, the chrysalis resembles part of the tree on which it hangs�or it looks a bit snakelike, depending on the vantage point. (Watch video: Growing Up Butterfly.)

This species'' caterpillar has some tricks up its sleeve: It can resemble bird droppings but can also look like a tiny snake at a later stage of development.

The monarch butterfly chrysalis has what appear to be gold dots and threads, which help the developing insect blend in with leaves.','Why Do Butterflies Have Such Vibrant Colors and Patterns?',to_timestamp('16-MAR-15 07.15.02.458000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('16-MAR-15','DD-MON-RR'));
Insert into NEWS.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (15,'Attack may have shattered royal sculptures from the ninth century B.C.','The ancient city of Nimrud is the latest target of Islamic militants now ravaging the cultural treasures of Iraq.

In a brief statement, the country''s Ministry of Tourism and Antiquities reported that ISIS had bulldozed the site, but it offered no information on the extent of the damage.

Spreading along the east bank of the Tigris River south of the modern city of Mosul, the site was one of four consecutive capitals of the Assyrian Empire.

"Nimrud is the modern name," says Nicholas Postgate, a professor of Assyriology at the University of Cambridge. "The ancient name was Kalhu. It''s mentioned in the Bible, under the spelling ''Calah.'' "

A city had already taken shape at this location by 1400 B.C., but in the early ninth century B.C. King Ashurnasirpal II made it into his new administrative capital, adding a five-mile-long wall, a monumental stepped tower called a ziggurat, new temples, and a large palace covered in elaborate decorations.

It''s those royal decorations that are of greatest concern. They consist of large stone panels of intricately carved reliefs that line the base of the building''s mud-brick walls. In boldly delineated detail, the panels show military campaigns, conquered peoples offering tribute to the king, ritual ceremonies undertaken by the king (sometimes alongside an ornamental, sacred tree), and many winged mythical figures known as geniis.

"I think there must be hundreds of meters of those reliefs," says Postgate. "Many of the rooms in the palace had them."','ISIS Bulldozes One-of-a-Kind Ancient Palace in Iraq',to_timestamp('16-MAR-15 07.17.42.856000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('16-MAR-15','DD-MON-RR'));
Insert into NEWS.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (16,'Villarrica erupted earlier this week with a bang, spewing lava bombs and forcing the evacuation of thousands of people.','Villarrica, one of Chile''s most active volcanoes, awoke with a vengeance this week, launching lava bombs and an ash cloud thousands of feet into the air. The nearby towns of Pucon and Conaripe were evacuated as a precaution.

This 9,330-foot-tall (2,850-meter) peak is known as a stratovolcano or a composite volcano, according to the Smithsonian''s Global Volcanism Program. They''re formed by an accumulation of lava and other debris shot out of cracks and craters during eruptions. (See stunning pictures of blue flames leaping from a volcano.)

A stratovolcano can have vents at its summit as well as along its flanks, and eruptions can occur through any one of them. More than 30 small cinder cones and vents dot the sides of Villarrica.

Major eruptions in 1985 and 1992 added lava layers and two new cones to Villarrica. Historical records of this volcano''s eruptions go back to 1558. (Watch an underwater volcano form a new island near Japan.)

This week''s eruption was characterized by so-called strombolian explosions, according to the Smithsonian. Caused by the sudden release of gases that have built up within a volcano, strombolian explosions result in spectacular fountains of lava and debris.

The ash clouds that balloon over these eruptions can also spawn lightning, thanks to the static electricity that builds up. (See lightning storms blossom over another Chilean volcano.)

These fireworks pose an additional potential hazard to the surrounding population in the form of mudslides and floods. Villarrica is covered with roughly 15 square miles (40 square kilometers) of glaciers, which are now being peppered with flaming chunks of melted rock. Rivers are rising as a result of the melting snow and ice, reports USA Today.','Stunning Nighttime Pictures of Chilean Volcano Erupting',to_timestamp('16-MAR-15 07.19.31.165000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('16-MAR-15','DD-MON-RR'));
Insert into NEWS.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (17,'Discovery pushes back the origin of our genus, Homo, by half a million years.','In a rare congruence of new evidence, two fossil jaws cast a fused beam of light on one of the darkest mysteries in human evolution: the origin of our genus Homo. The two lower jaws�one a reconstruction of a pivotal specimen found half a century ago, the other freshly plucked from the badlands of Ethiopia�point to East Africa as the birthplace of our evolutionary lineage.

The new Ethiopian fossil, announced online by the journal Science, pushes the arrival of Homo on the East African landscape back almost half a million years, to 2.8 million years ago. The date is tantalizingly close to the last known appearance, around three million years ago, of Australopithecus afarensis, an upright-walking, small-brained species best known from the skeleton called Lucy, believed by many scientists to be the direct ancestor of our genus. The new jaw, known as LD 350-1, was found in January 2013 just a dozen miles from where Lucy was found in 1974.

"This is exciting stuff," says paleoanthropologist Donald Johanson, who discovered Lucy.

The Afar, part of the East African Rift Valley, has yielded many other prize fossils of hominins�members of the extended human family�including the previous earliest known Homo specimen, an upper jaw known as AL 666-1, dated to 2.3 million years ago. (Learn more about Ardi, another human ancestor from the Afar region.)

Fossils attributed to Homo in the period two to three million years ago are exceedingly rare. Bill Kimbel, director of the Institute of Human Origins at Arizona State University, in Tempe, who co-led the analysis of the new specimen, once said that "You could put them all into a small shoe box and still have room for a good pair of shoes." (See more about the hunt for fossils of humans'' early ancestors.)','Oldest Human Fossil Found, Redrawing Family Tree',to_timestamp('16-MAR-15 07.20.48.821000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('16-MAR-15','DD-MON-RR'));
Insert into NEWS.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (18,'Female killer whales that live long past their "child-bearing" days may be valuable sources of information for their groups.','Like people, female killer whales undergo menopause, living long after their baby-bearing days are done. But why people, and whales, do so is very much up for debate. Now, a new study says the reason�at least in killer whales�may have to do with who holds information critical for the group''s survival.


Researchers found evidence that menopausal whales act as a kind of library of information, directing their groups or pods to where they can find food when fish are scarce. In this way, older females give their pods a greater chance at survival, according to the study, published March 5 in the journal Current Biology.

Females stop having calves when they''re around 40 years old, but they can live to be 90. Males live to be about 50 years old.

"The pioneers of killer whale ecology have long felt that matriarchs serve as repositories of traditional ecological knowledge that can help these whales survive through years of low prey abundance," says whale biologist Rob Williams, a Pew Fellow in marine conservation who was not involved in the research, in an email.

"This study tests that theory and offers a compelling hint that whales, especially sons, follow the matriarch to find food."

Listening to Your Elders

Darren Croft, a behavioral ecologist at the University of Exeter in the U.K., and colleagues looked at southern resident killer whales�groups of about 80 animals that spend their time in the waters around Seattle, Washington, and subsist mainly on salmon.

The team examined observational data for southern resident killer whales taken since 1976 to determine if menopausal whales acted as leaders during group movements in and out of their foraging areas.','After Menopause, Female Killer Whales Help Pod Survive',to_timestamp('16-MAR-15 07.22.04.188000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('16-MAR-15','DD-MON-RR'));
Insert into NEWS.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (19,'A galaxy''s alignment lets astronomers watch a strange, distant supernova explode. And then watch it again. And again. And again and again.','Far, far away, a galaxy is acting as a cosmic split-screen. Parked in front of an exploding star, the galaxy''s gravity is projecting magnified images of the supernova onto the sky�four images, to be precise, in a celestial mirage called an Einstein Cross.

The weirdness doesn''t end there, though. Within the next decade, the cosmos will replay that exact same explosion, in a different spot in the sky.

Called a multiply lensed supernova, the strange object was spotted in late 2014 and reported Thursday in Science. And it''s still getting brighter. Studying it, and the predicted rerun, will let astronomers combine a few of the things they love: exploding stars, dark matter, and the expanding universe, none of which are particularly well understood.

"You just see the four dots and they form this beautiful Einstein Cross, and it''s just kind of spooky," says Daniel Holz, an astrophysicist at the University of Chicago.

Gravitational Lensing

Instead of intergalactic voodoo, the twisted apparitions are the result of a fortuitous cosmic alignment.

Exactly one century ago, in his theory of general relativity, Einstein described how massive objects could tug on light, and even pull at the fabric of space-time. Put simply, strong gravitational fields can act as lenses-warping, magnifying, and redirecting light as it travels through the cosmos. (See another supernova made superbright by gravitational lensing.)

"It''s just this incredible verification of general relativity," says study co-author Ryan Foley of the University of Illinois at Urbana-Champaign. (Learn how a NASA gravity probe confirmed two more of Einstein''s predictions.)

Depending on the lensing geometry and position of the viewer�in this case, the Hubble Space Telescope�multiple images of the same background object can be produced. Some of those images might even arrive years apart.','Galaxy in Front of Supernova Creates Cosmic Mirage',to_timestamp('16-MAR-15 07.24.07.637000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('16-MAR-15','DD-MON-RR'));
Insert into NEWS.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (20,'The ancient sea covered a fifth of the planet''s surface, to a depth of at least a mile.','Astronomers using some of the world''s most powerful telescopes have determined that an ocean at least a mile deep covered a significant fraction of the Martian surface four billion years ago.

The research, reported Thursday in the journal Science, reinforces reams of earlier evidence that water once existed on the surface of the red planet, leaving traces such as stream pebbles, ancient shorelines, river deltas, minerals that must have formed in a watery environment, and more. (Video: NASA''s Mars Phoenix once detected snow falling on Mars.)

This time, the evidence comes from an analysis of the water vapor that lingers today in the Martian atmosphere. A team of scientists led by Geronimo Villanueva, of NASA''s Goddard Space Flight Center, took a series of measurements of sunlight reflecting off the Martian surface and through the atmosphere, which revealed the chemical composition of the atmosphere''s vapor. The hydrogen in water, whether it''s on Mars or on Earth, comes in two varieties: ordinary hydrogen, whose nuclei contain just a proton, and "heavy" hydrogen, also known as deuterium, with a proton and a neutron.

Both Earth and Mars started out with identical proportions of ordinary hydrogen and deuterium in their water. But on Mars, with its weaker gravity, the ordinary hydrogen has been gradually leaking from the atmosphere out into space. As a result, Mars now has a higher proportion of deuterium in its water than Earth does�seven times as high, in fact. Since the scientists can estimate the escape rate that would have led to this imbalance, Geronimo and his co-authors can calculate how much water overall there once was.

It would have been enough, they say, that if it had covered the entire planet evenly, it would have reached a depth of about 450 feet (137 meters). In practice, says Geronimo, "it probably would have formed an ocean in the northern hemisphere, which has a consistently lower altitude."
','Astronomers Find a Long-Lost Ocean on Mars',to_timestamp('16-MAR-15 07.26.34.399000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_date('16-MAR-15','DD-MON-RR'));


Insert into NEWS.NEWS_TAG (NEWS_TAG_ID,NEWS_ID,TAG_ID) values (1,1,6);
Insert into NEWS.NEWS_TAG (NEWS_TAG_ID,NEWS_ID,TAG_ID) values (2,19,1);
Insert into NEWS.NEWS_TAG (NEWS_TAG_ID,NEWS_ID,TAG_ID) values (3,6,12);
Insert into NEWS.NEWS_TAG (NEWS_TAG_ID,NEWS_ID,TAG_ID) values (4,19,6);
Insert into NEWS.NEWS_TAG (NEWS_TAG_ID,NEWS_ID,TAG_ID) values (5,14,13);


Insert into NEWS.TAG (TAG_ID,TAG_NAME) values (1,'space');
Insert into NEWS.TAG (TAG_ID,TAG_NAME) values (2,'sun');
Insert into NEWS.TAG (TAG_ID,TAG_NAME) values (3,'science');
Insert into NEWS.TAG (TAG_ID,TAG_NAME) values (4,'animal');
Insert into NEWS.TAG (TAG_ID,TAG_NAME) values (5,'wolfe');
Insert into NEWS.TAG (TAG_ID,TAG_NAME) values (6,'astronomy');
Insert into NEWS.TAG (TAG_ID,TAG_NAME) values (12,'planet');
Insert into NEWS.TAG (TAG_ID,TAG_NAME) values (13,'butterflies');
Insert into NEWS.TAG (TAG_ID,TAG_NAME) values (14,'Butterflies');


Insert into NEWS.NEWS_TAG_NAME (NEWS_TAG_ID,NEWS_ID,TAG_NAME) values (1,1,'astronomy');
Insert into NEWS.NEWS_TAG_NAME (NEWS_TAG_ID,NEWS_ID,TAG_NAME) values (2,19,'space');
Insert into NEWS.NEWS_TAG_NAME (NEWS_TAG_ID,NEWS_ID,TAG_NAME) values (3,6,'planet');
Insert into NEWS.NEWS_TAG_NAME (NEWS_TAG_ID,NEWS_ID,TAG_NAME) values (4,19,'astronomy');
Insert into NEWS.NEWS_TAG_NAME (NEWS_TAG_ID,NEWS_ID,TAG_NAME) values (5,14,'butterflies');